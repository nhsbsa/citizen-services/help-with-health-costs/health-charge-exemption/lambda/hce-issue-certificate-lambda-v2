#!/bin/bash

npm run build
echo "copying node_modules to /dist folder"
cp -R node_modules dist
echo "zipping /dist folder"
zip -9 -r -q hce-issue-certificate-lambda-v2.zip dist
echo "copying zip to /hrt-ppc-localstack folder"
cp hce-issue-certificate-lambda-v2.zip ../hrt-ppc-localstack/lambda-archives
echo "removing zip"
rm -f hce-issue-certificate-lambda-v2.zip
echo "removing node_modules from /dist folder"
rm -rf dist/node_modules