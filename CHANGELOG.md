# HCE issue certificate Lambda v2 Changelog

## v0.0.0 - 13 March 2024

:new: **New features**

- {feature}

:wrench: **Fixes**

- {issue}
