process.env.NODE_ENV = "local";
process.env.outBoundApiTimeout = "10000";
process.env.logApiRequestAndResponse = "true";
import { logger } from "@nhsbsa/health-charge-exemption-npm-logging";
import {
  CitizenApi,
  CertificateApi,
  UserPreferenceApi,
} from "@nhsbsa/health-charge-exemption-npm-utils-rest";

export const winstonInfoLoggerSpy: jest.SpyInstance = jest.spyOn(
  logger,
  "info",
);
export const citizenApiSpyInstance: jest.SpyInstance = jest.spyOn(
  CitizenApi.prototype,
  "makeRequest",
);
export const certificateApiSpyInstance: jest.SpyInstance = jest.spyOn(
  CertificateApi.prototype,
  "makeRequest",
);
export const userPreferenceApiSpyInstance: jest.SpyInstance = jest.spyOn(
  UserPreferenceApi.prototype,
  "makeRequest",
);
