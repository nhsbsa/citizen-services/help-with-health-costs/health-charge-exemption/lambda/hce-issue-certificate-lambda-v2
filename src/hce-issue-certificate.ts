import { loggerWithContext } from "@nhsbsa/health-charge-exemption-npm-logging";
import { MandatoryHeaders } from "@nhsbsa/health-charge-exemption-npm-utils-rest";
import {
  CertificateSchema,
  CitizenBaseSchema,
  IssueCertificateResponseObject,
} from "./schemas";
import {
  createCertificateRequestData,
  persistCertificate,
} from "./utils/api/certificate";
import { persistCitizen } from "./utils/api/citizen";
import { handleCitizenMatch } from "./utils/helpers/matching-utils";
import {
  CITIZEN_FIELD_KEY,
  CitizenUserPreferenceResponse,
  OptionalCitizenResponse,
  PARTNER_FIELD_KEY,
  createCitizenPromiseForId,
  createUserPreferencePromise,
  parseUserPreferenceForCitizenResponse,
} from "./utils/helpers/common";
import { CitizenGetResponse } from "@nhsbsa/health-charge-exemption-npm-common-models";

export async function issueCertificate(
  headers: MandatoryHeaders,
  citizen: CitizenBaseSchema,
  partner: CitizenBaseSchema | null,
  certificate: CertificateSchema,
) {
  loggerWithContext().info("Entered issue certificate service");

  const [citizenId, partnerId] = [citizen.id, partner?.id];

  // conditionally add to promise array based on values
  const citizenPromises: Promise<CitizenUserPreferenceResponse>[] = [
    ...createCitizenPromiseForId(citizenId, certificate.type, headers),
    ...createCitizenPromiseForId(partnerId, certificate.type, headers),
  ];

  const [
    existingCitizen,
    existingCitizenPreference,
    existingPartnerCitizen,
    existingPartnerPreference,
  ] = await Promise.all(citizenPromises);

  let citizenResponse: OptionalCitizenResponse = null;
  if (existingCitizen) {
    citizenResponse = handleCitizenMatch(
      existingCitizen as CitizenGetResponse,
      citizen,
      CITIZEN_FIELD_KEY,
    );
  }

  let partnerResponse: OptionalCitizenResponse = null;
  if (existingPartnerCitizen) {
    partnerResponse = handleCitizenMatch(
      existingPartnerCitizen as CitizenGetResponse,
      partner,
      PARTNER_FIELD_KEY,
    );
  }

  if (!citizenResponse) {
    citizenResponse = await persistCitizen(citizen, headers);
    loggerWithContext().info(
      `Citizen record saved successfully, citizenId:[${citizenResponse.id}]`,
    );
  }

  if (!partnerResponse && partner) {
    partnerResponse = await persistCitizen(partner, headers);
    loggerWithContext().info(
      `Citizen record saved for partner successfully, citizenId:[${partnerResponse.id}]`,
    );
  }

  const postApiPromises = [
    persistCertificate(
      createCertificateRequestData(partnerResponse, certificate),
      citizenResponse.id,
      headers,
    ).then((response) => {
      loggerWithContext().info(
        `Certificate record saved successfully, certificateId:[${response.id}, reference: [${response.reference}]]`,
      );
      return response;
    }),
  ];

  // Handle citizen user preference
  postApiPromises.push(
    createUserPreferencePromise(
      citizenResponse,
      citizen,
      existingCitizenPreference,
      certificate,
      headers,
    ),
  );

  // Handle partner user preference if partner exists
  if (partner) {
    postApiPromises.push(
      createUserPreferencePromise(
        partnerResponse,
        partner,
        existingPartnerPreference,
        certificate,
        headers,
      ),
    );
  }

  const [
    certificateResponse,
    userPreferenceResponse,
    partnerUserPreferenceResponse,
  ] = await Promise.all(postApiPromises);

  const citizenReponseObject = parseUserPreferenceForCitizenResponse(
    citizenResponse,
    userPreferenceResponse,
    existingCitizenPreference,
  );

  const responseObject: IssueCertificateResponseObject = {
    citizen: citizenReponseObject,
    certificate: certificateResponse,
  };

  if (partner) {
    responseObject.partner = parseUserPreferenceForCitizenResponse(
      partnerResponse,
      partnerUserPreferenceResponse,
      existingPartnerPreference,
    );
  }

  return responseObject;
}
