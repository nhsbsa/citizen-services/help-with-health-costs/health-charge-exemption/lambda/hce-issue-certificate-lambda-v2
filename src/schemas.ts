import { z } from "zod";
import { PARTNER_FIELD_KEY } from "./utils/helpers/common";
import {
  Preference,
  CertificateType,
} from "@nhsbsa/health-charge-exemption-npm-common-models";

/**
 * For reference:
 * Lots of the fields within each schema are marked as strings instead of their intended types.
 * This is because the APIs downstream should validate these values and we want to keep the lambda as lightweight as possible with only the necessary validation.
 */

export const addressSchema = z.object({
  addressLine1: z.string(),
  addressLine2: z.string().optional(),
  townOrCity: z.string(),
  country: z.string().optional(),
  postcode: z.string(),
});

export const userPreferenceSchema = z.object({
  preference: z.nativeEnum(Preference),
});

export const emailSchema = z.object({
  emailAddress: z.string().optional(),
});

export const certificateSchema = z.object({
  type: z.string().optional(),
  duration: z.string().optional(),
  cost: z.number().optional(),
  startDate: z.string().optional(),
  endDate: z.string().optional(),
  applicationDate: z.string().optional(),
  pharmacyId: z.string().optional(),
  productId: z.string().min(1, "must not be null or empty"),
  reference: z.string().optional(),
  lowIncomeScheme: z
    .object({
      partnerCitizenId: z.string().optional(),
      excessIncome: z.number().optional(),
    })
    .optional(),
});

// Fields are optional here as we dont currently validate them individually.
export const citizenBaseSchema = z
  .object({
    id: z.string().optional(),
    firstName: z.string().optional(),
    lastName: z.string().optional(),
    dateOfBirth: z.string().optional(),
    nhsNumber: z.string().optional(),
    addresses: z.array(addressSchema).optional(),
    emails: z.array(emailSchema).optional(),
    userPreference: userPreferenceSchema,
  })
  .refine(
    (data) => {
      return (
        data.userPreference.preference !== Preference.EMAIL ||
        data.emails?.some((email) => email.emailAddress)
      );
    },
    {
      message: "user preference set to EMAIL but no email address provided",
    },
  );

export const comparisonSchema = z.object({
  firstName: z.string().optional(),
  lastName: z.string().optional(),
  dateOfBirth: z.string().optional(),
  nhsNumber: z.string().optional(),
  addresses: z.array(addressSchema).optional(),
  emails: z
    .array(emailSchema)
    .optional()
    .transform((emails) => emails ?? []), // we transform here for matching as db citizen has empty array
});

export const citizenBaseResponseSchema = z.object({
  id: z.string(),
  firstName: z.string(),
  lastName: z.string(),
  dateOfBirth: z.string(),
  nhsNumber: z.string().optional(),
  userPreference: z.object({
    id: z.string(),
    citizenId: z.string(),
    certificateType: z.string(),
    event: z.string(),
    preference: z.string(),
    _meta: z.any(),
    _links: z.any(),
  }),
  _meta: z.any(), // no need to parse individual parts of the _meta
  _links: z.any(), // we don't need to parse or mutate links so we just pass them through
});

/**
 * A ZOD schema for validating a payload containing citizen, certificate, and partner information.
 *
 * @constant {ZodObject} payloadSchema
 * @property {ZodObject} citizen - The schema for the citizen's information.
 * @property {ZodObject} certificate - The schema for the certificate information.
 * @property {ZodObject} partner - The schema for the partner's information.
 *
 * @example usage which will validate the payload object.
 * payloadSchema.parse(payload);
 */
export const payloadSchema = z
  .object({
    citizen: citizenBaseSchema,
    partner: citizenBaseSchema.optional(),
    certificate: certificateSchema,
  })
  .refine(
    (data) => {
      if (
        data.partner &&
        ![CertificateType.LIS_HC2, CertificateType.LIS_HC3].includes(
          data.certificate.type as CertificateType,
        )
      ) {
        return false;
      }
      return true;
    },
    {
      message: "partner only applies to LIS_HC2 or LIS_HC3 certificate types",
      path: [PARTNER_FIELD_KEY],
    },
  );

export type CitizenBaseResponseSchema = z.infer<
  typeof citizenBaseResponseSchema
>;
export type CertificateSchema = z.infer<typeof certificateSchema>;
export type CitizenBaseSchema = z.infer<typeof citizenBaseSchema>;
export type PayloadBody = z.infer<typeof payloadSchema>;
export type IssueCertificateResponseObject = {
  citizen: CitizenBaseResponseSchema;
  partner?: CitizenBaseResponseSchema;
  certificate: object; // would use certificateResponse but schema values can be string
};
