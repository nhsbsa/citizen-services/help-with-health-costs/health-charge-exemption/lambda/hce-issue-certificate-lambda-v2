import {
  MandatoryHeaders,
  UserPreferenceApi,
} from "@nhsbsa/health-charge-exemption-npm-utils-rest";
import { CertificateSchema, CitizenBaseSchema } from "../../schemas";
import { getEvent } from "../helpers/common";
import {
  UserPreferencePostResponse,
  UserPreferencePatchResponse,
  CertificateType,
  UserPreferenceGetResponse,
  UserPreferencePostRequest,
  UserPreferencePatchRequest,
  Preference,
} from "@nhsbsa/health-charge-exemption-npm-common-models";

const userPreferenceApi = new UserPreferenceApi();

export async function persistUserPreference(
  userPreference: UserPreferencePostRequest,
  citizenId: string,
  headers: MandatoryHeaders,
): Promise<UserPreferencePostResponse> {
  return userPreferenceApi.makeRequest({
    method: "POST",
    url: "/v1/user-preference",
    params: { citizenId },
    data: userPreference,
    headers,
    responseType: "json",
  });
}

export async function patchUserPreference(
  userPreferencePatchBody: UserPreferencePatchRequest,
  userPreferenceId: string,
  headers: MandatoryHeaders,
): Promise<UserPreferencePatchResponse> {
  return userPreferenceApi.makeRequest({
    method: "PATCH",
    url: "/v1/user-preference/" + userPreferenceId,
    data: userPreferencePatchBody,
    headers,
    responseType: "json",
  });
}

export async function findUserPreferenceForCitizen(
  certificateType: CertificateSchema["type"] | CertificateType,
  citizenId: string,
  headers: MandatoryHeaders,
): Promise<UserPreferenceGetResponse> {
  return userPreferenceApi.makeRequest({
    method: "GET",
    url: "/v1/user-preference/search/findByCitizenIdAndCertificateType",
    params: { citizenId, certificateType },
    headers,
    responseType: "json",
  });
}

export function createUserPreferencePostData(
  certificate: CertificateSchema,
  citizen: CitizenBaseSchema,
  headers: MandatoryHeaders,
): UserPreferencePostRequest {
  return {
    certificateType: certificate.type as CertificateType,
    event: getEvent(headers, citizen),
    preference: <Preference>citizen.userPreference.preference,
  };
}

export function createUserPreferencePatchData(
  citizen: CitizenBaseSchema,
  headers: MandatoryHeaders,
): UserPreferencePatchRequest {
  return {
    event: getEvent(headers, citizen),
    preference: <Preference>citizen.userPreference.preference,
  };
}
