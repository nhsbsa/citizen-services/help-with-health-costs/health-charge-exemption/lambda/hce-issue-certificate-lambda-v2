import {
  createCertificateRequestData,
  persistCertificate,
} from "../certificate";
import { CertificateSchema } from "../../../schemas";
import {
  CERTIFICATE_BASE_URL_PATH,
  MOCK_CITIZEN_ID,
  mockMandatoryHeaders,
  mockedError,
} from "../../../__tests__/utils/test-constants";
import { certificateApiSpyInstance } from "../../../../test-setup/setup";
import { mockCertificateResponse } from "../../../__mocks__/certificate/certificate-mocks";
import { buildExpectedPostRequest } from "../../../__tests__/utils/test-functions";
import {
  CertificateType,
  CitizenGetResponse,
} from "@nhsbsa/health-charge-exemption-npm-common-models";

const mockCertificate = {
  type: CertificateType.LIS_HC2,
  lowIncomeScheme: {
    excessIncome: 100,
  },
} as unknown as CertificateSchema;

const mockPartnerResponse = {
  id: "partner-id",
} as CitizenGetResponse;

describe("createCertificateRequestData", () => {
  it.each([CertificateType.LIS_HC2, CertificateType.LIS_HC3])(
    "should return correct certificate data with the partnerCitizenId when partnerResponse is provided and certificate type is %s",
    (certificateType: CertificateType) => {
      // given
      mockCertificate.type = certificateType;

      // when
      const result = createCertificateRequestData(
        mockPartnerResponse,
        mockCertificate,
      );

      // then
      expect(result).toEqual({
        ...mockCertificate,
        lowIncomeScheme: {
          ...mockCertificate.lowIncomeScheme,
          partnerCitizenId: mockPartnerResponse.id,
        },
      });
    },
  );

  it("should return the certificate data unchanged when partnerResponse is provided but certificate type is not LIS_HC2 or LIS_HC3", () => {
    // given
    const mockCertificateWithHrtPpc = {
      type: CertificateType.HRT_PPC,
    } as CertificateSchema;

    // when
    const result = createCertificateRequestData(
      mockPartnerResponse,
      mockCertificateWithHrtPpc,
    );

    // then
    expect(result).toEqual(mockCertificateWithHrtPpc);
  });

  it.each([null, undefined])(
    "should return the certificate data unchanged when partnerResponse is %s",
    (partnerResponse: null | undefined) => {
      // given / when
      const result = createCertificateRequestData(
        partnerResponse,
        mockCertificate,
      );

      // then
      expect(result).toEqual(mockCertificate);
    },
  );
});

describe("persistCertificate", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  afterEach(() => {
    // then
    expect(certificateApiSpyInstance).toHaveBeenCalledTimes(1);
    expect(certificateApiSpyInstance).toHaveBeenCalledWith({
      ...buildExpectedPostRequest(
        CERTIFICATE_BASE_URL_PATH,
        mockCertificate,
        mockMandatoryHeaders,
      ),
      params: { citizenId: MOCK_CITIZEN_ID },
    });
  });

  it("should call certificateApi.makeRequest with the correct parameters", async () => {
    // given
    certificateApiSpyInstance.mockReturnValue(mockCertificateResponse);

    // when
    await persistCertificate(
      mockCertificate,
      MOCK_CITIZEN_ID,
      mockMandatoryHeaders,
    );
  });

  it("should handle errors thrown by certificateApi.makeRequest", async () => {
    // given
    certificateApiSpyInstance.mockRejectedValue(mockedError);

    // when / then
    await expect(
      persistCertificate(
        mockCertificate,
        MOCK_CITIZEN_ID,
        mockMandatoryHeaders,
      ),
    ).rejects.toThrow(mockedError);
  });
});
