import { userPreferenceApiSpyInstance } from "../../../../test-setup/setup";
import {
  MOCK_CITIZEN_ID,
  MOCK_USER_PREFERENCE_ID,
  USER_PREFERENCE_BASE_URL_PATH,
  USER_PREFERENCE_FIND_BY_CITIZEN_ID_AND_TYPE,
  mockPostUserPreferenceRequest,
  mockMandatoryHeaders,
  mockedError,
  mockPatchUserPreferenceRequest,
} from "../../../__tests__/utils/test-constants";
import {
  buildExpectedGetRequest,
  buildExpectedPatchRequest,
  buildExpectedPostRequest,
} from "../../../__tests__/utils/test-functions";
import { CertificateSchema, CitizenBaseSchema } from "../../../schemas";
import {
  createUserPreferencePatchData,
  createUserPreferencePostData,
  findUserPreferenceForCitizen,
  patchUserPreference,
  persistUserPreference,
} from "../user-preference";
import { getEvent } from "../../helpers/common";
import { mockUserPreferenceResponse } from "../../../__mocks__/user-preference/user-preference-mocks";
import {
  CertificateType,
  Event,
  Preference,
} from "@nhsbsa/health-charge-exemption-npm-common-models";

describe("persistUserPreference", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  afterEach(() => {
    // then
    expect(userPreferenceApiSpyInstance).toHaveBeenCalledTimes(1);
    expect(userPreferenceApiSpyInstance).toHaveBeenCalledWith({
      ...buildExpectedPostRequest(
        USER_PREFERENCE_BASE_URL_PATH,
        mockPostUserPreferenceRequest,
        mockMandatoryHeaders,
      ),
      params: { citizenId: MOCK_CITIZEN_ID },
    });
  });

  it("should call userPreferenceApi.makeRequest with the correct parameters", async () => {
    // given
    userPreferenceApiSpyInstance.mockReturnValue(mockUserPreferenceResponse);

    // when
    await persistUserPreference(
      mockPostUserPreferenceRequest,
      MOCK_CITIZEN_ID,
      mockMandatoryHeaders,
    );
  });

  it("should handle errors thrown by userPreferenceApi.makeRequest", async () => {
    // given
    userPreferenceApiSpyInstance.mockRejectedValue(mockedError);

    // when / then
    await expect(
      persistUserPreference(
        mockPostUserPreferenceRequest,
        MOCK_CITIZEN_ID,
        mockMandatoryHeaders,
      ),
    ).rejects.toThrow(mockedError);
  });
});

describe("patchUserPreference", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  afterEach(() => {
    // then
    expect(userPreferenceApiSpyInstance).toHaveBeenCalledTimes(1);
    expect(userPreferenceApiSpyInstance).toHaveBeenCalledWith({
      ...buildExpectedPatchRequest(
        USER_PREFERENCE_BASE_URL_PATH + "/" + MOCK_USER_PREFERENCE_ID,
        mockPatchUserPreferenceRequest,
        mockMandatoryHeaders,
      ),
    });
  });

  it("should call userPreferenceApi.makeRequest with the correct parameters", async () => {
    // given
    userPreferenceApiSpyInstance.mockReturnValue(mockUserPreferenceResponse);

    // when
    await patchUserPreference(
      mockPatchUserPreferenceRequest,
      MOCK_USER_PREFERENCE_ID,
      mockMandatoryHeaders,
    );
  });

  it("should handle errors thrown by userPreferenceApi.makeRequest", async () => {
    // given
    userPreferenceApiSpyInstance.mockRejectedValue(mockedError);

    // when / then
    await expect(
      patchUserPreference(
        mockPatchUserPreferenceRequest,
        MOCK_USER_PREFERENCE_ID,
        mockMandatoryHeaders,
      ),
    ).rejects.toThrow(mockedError);
  });
});

describe("findUserPreferenceForCitizen", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  afterEach(() => {
    // then
    expect(userPreferenceApiSpyInstance).toHaveBeenCalledTimes(1);
    expect(userPreferenceApiSpyInstance).toHaveBeenCalledWith({
      ...buildExpectedGetRequest(
        USER_PREFERENCE_FIND_BY_CITIZEN_ID_AND_TYPE,
        mockMandatoryHeaders,
      ),
      params: {
        citizenId: MOCK_CITIZEN_ID,
        certificateType: CertificateType.HRT_PPC,
      },
    });
  });

  it("should call userPreferenceApi.makeRequest with the correct parameters", async () => {
    // given
    userPreferenceApiSpyInstance.mockReturnValue(mockUserPreferenceResponse);

    // when
    await findUserPreferenceForCitizen(
      CertificateType.HRT_PPC,
      MOCK_CITIZEN_ID,
      mockMandatoryHeaders,
    );
  });

  it("should handle errors thrown by userPreferenceApi.makeRequest", async () => {
    // given
    userPreferenceApiSpyInstance.mockRejectedValue(mockedError);

    // when / then
    await expect(
      findUserPreferenceForCitizen(
        CertificateType.HRT_PPC,
        MOCK_CITIZEN_ID,
        mockMandatoryHeaders,
      ),
    ).rejects.toThrow(mockedError);
  });
});

jest.mock("../../helpers/common", () => ({
  getEvent: jest.fn(),
}));

describe("preference request data utilities", () => {
  beforeEach(() => {
    jest.clearAllMocks();
    (getEvent as jest.Mock).mockReturnValue(Event.ANY);
  });

  describe("createUserPreferencePostData", () => {
    test.each([
      {
        certificateType: CertificateType.LIS_HC2,
        preference: Preference.EMAIL,
      },
      {
        certificateType: CertificateType.LIS_HC2,
        preference: Preference.POSTAL,
      },
      {
        certificateType: CertificateType.LIS_HC3,
        preference: Preference.EMAIL,
      },
      {
        certificateType: CertificateType.LIS_HC3,
        preference: Preference.POSTAL,
      },
      {
        certificateType: CertificateType.HRT_PPC,
        preference: Preference.EMAIL,
      },
      {
        certificateType: CertificateType.HRT_PPC,
        preference: Preference.POSTAL,
      },
    ])(
      "should return post data object with correct params %s",
      ({ certificateType, preference }) => {
        // given
        const certificate: CertificateSchema = {
          type: certificateType,
        } as CertificateSchema;
        const citizen: CitizenBaseSchema = {
          userPreference: { preference: preference },
        };

        // when
        const result = createUserPreferencePostData(
          certificate,
          citizen,
          mockMandatoryHeaders,
        );

        // then
        expect(result).toEqual({
          ...mockPostUserPreferenceRequest,
          certificateType: certificateType,
          preference: preference,
        });
        expect(getEvent).toHaveBeenCalledWith(mockMandatoryHeaders, citizen);
      },
    );
  });

  describe("createUserPreferencePatchData", () => {
    beforeEach(() => {
      jest.clearAllMocks();
      (getEvent as jest.Mock).mockReturnValue(Event.ANY);
    });

    test.each([Preference.EMAIL, Preference.POSTAL])(
      "should return patch data object with correct params when preference is %s",
      (preference: Preference) => {
        // given
        const citizen: CitizenBaseSchema = {
          userPreference: { preference: preference },
        };

        // when
        const result = createUserPreferencePatchData(
          citizen,
          mockMandatoryHeaders,
        );

        // then
        expect(result).toEqual({
          ...mockPatchUserPreferenceRequest,
          preference: preference,
        });
        expect(getEvent).toHaveBeenCalledWith(mockMandatoryHeaders, citizen);
      },
    );
  });
});
