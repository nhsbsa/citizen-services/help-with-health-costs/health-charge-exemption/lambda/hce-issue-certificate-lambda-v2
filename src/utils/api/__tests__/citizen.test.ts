import { citizenApiSpyInstance } from "../../../../test-setup/setup";
import { mockCitizenResponse } from "../../../__mocks__/citizen/citizen-mocks";
import {
  CITIZEN_BASE_URL_PATH,
  MOCK_CITIZEN_ID,
  citizenBaseSchemaMock,
  mockMandatoryHeaders,
  mockedError,
} from "../../../__tests__/utils/test-constants";
import {
  buildExpectedGetRequest,
  buildExpectedPostRequest,
} from "../../../__tests__/utils/test-functions";
import { getCitizenById, persistCitizen } from "../citizen";

describe("persistCitizen", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  afterEach(() => {
    // then
    expect(citizenApiSpyInstance).toHaveBeenCalledTimes(1);
    expect(citizenApiSpyInstance).toHaveBeenCalledWith(
      buildExpectedPostRequest(
        CITIZEN_BASE_URL_PATH,
        citizenBaseSchemaMock,
        mockMandatoryHeaders,
      ),
    );
  });

  it("should call citizenApi.makeRequest with the correct parameters", async () => {
    // given
    citizenApiSpyInstance.mockReturnValue(mockCitizenResponse);

    // when
    await persistCitizen(citizenBaseSchemaMock, mockMandatoryHeaders);
  });

  it("should handle errors thrown by citizenApi.makeRequest", async () => {
    // given
    citizenApiSpyInstance.mockRejectedValue(mockedError);

    // when / then
    await expect(
      persistCitizen(citizenBaseSchemaMock, mockMandatoryHeaders),
    ).rejects.toThrow(mockedError);
  });
});

describe("getCitizenById", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  afterEach(() => {
    // then
    expect(citizenApiSpyInstance).toHaveBeenCalledTimes(1);
    expect(citizenApiSpyInstance).toHaveBeenCalledWith(
      buildExpectedGetRequest(
        CITIZEN_BASE_URL_PATH + "/" + MOCK_CITIZEN_ID,
        mockMandatoryHeaders,
      ),
    );
  });

  it("should call citizenApi.makeRequest with the correct parameters", async () => {
    // given
    citizenApiSpyInstance.mockReturnValue(mockCitizenResponse);

    // when
    await getCitizenById(MOCK_CITIZEN_ID, mockMandatoryHeaders);
  });

  it("should handle errors thrown by citizenApi.makeRequest", async () => {
    // given
    citizenApiSpyInstance.mockRejectedValue(mockedError);

    // when / then
    await expect(
      getCitizenById(MOCK_CITIZEN_ID, mockMandatoryHeaders),
    ).rejects.toThrow(mockedError);
  });
});
