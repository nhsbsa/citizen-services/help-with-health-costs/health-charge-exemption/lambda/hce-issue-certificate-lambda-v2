import {
  CitizenApi,
  MandatoryHeaders,
} from "@nhsbsa/health-charge-exemption-npm-utils-rest";
import { CitizenBaseSchema } from "../../schemas";
import { CitizenGetResponse } from "@nhsbsa/health-charge-exemption-npm-common-models";

const citizenApi = new CitizenApi();

export async function persistCitizen(
  citizen: CitizenBaseSchema,
  headers: MandatoryHeaders,
): Promise<CitizenGetResponse> {
  return citizenApi.makeRequest({
    method: "POST",
    url: "/v1/citizens",
    data: citizen,
    headers,
    responseType: "json",
  });
}

export async function getCitizenById(
  citizenId: string,
  headers: MandatoryHeaders,
): Promise<CitizenGetResponse> {
  return citizenApi.makeRequest({
    method: "GET",
    url: `/v1/citizens/${citizenId}`,
    headers,
    responseType: "json",
  });
}
