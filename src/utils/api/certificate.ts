import {
  CertificateApi,
  MandatoryHeaders,
} from "@nhsbsa/health-charge-exemption-npm-utils-rest";
import { CertificateSchema } from "../../schemas";
import {
  CertificateType,
  CitizenGetResponse,
} from "@nhsbsa/health-charge-exemption-npm-common-models";

const certificateApi = new CertificateApi();

export async function persistCertificate(
  certificate: CertificateSchema,
  citizenId: string,
  headers: MandatoryHeaders,
) {
  return certificateApi.makeRequest({
    method: "POST",
    url: "/v1/certificates",
    params: { citizenId },
    data: certificate,
    headers,
    responseType: "json",
  });
}

export function createCertificateRequestData(
  partnerResponse: CitizenGetResponse | null | undefined,
  certificate: CertificateSchema,
): CertificateSchema {
  let lowIncomeScheme = certificate.lowIncomeScheme;
  if (
    partnerResponse &&
    [CertificateType.LIS_HC2, CertificateType.LIS_HC3].includes(
      certificate.type as CertificateType,
    )
  ) {
    lowIncomeScheme = {
      ...lowIncomeScheme,
      partnerCitizenId: partnerResponse.id,
    };
  }

  return {
    ...certificate,
    lowIncomeScheme,
  };
}
