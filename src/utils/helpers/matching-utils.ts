import { z } from "zod";
import {
  CitizenBaseSchema,
  citizenBaseSchema,
  comparisonSchema,
} from "../../schemas";
import {
  FieldError,
  BadRequest,
} from "@nhsbsa/health-charge-exemption-npm-utils-rest";
import { FieldPrefix } from "./common";
import { loggerWithContext } from "@nhsbsa/health-charge-exemption-npm-logging";
import { diff } from "just-diff";
import { CitizenGetResponse } from "@nhsbsa/health-charge-exemption-npm-common-models";

const RECORD_MISMATCH_VALIDATION_MESSAGE =
  "The provided details do not match with the existing citizen record";

export function compareCitizenObjectsUsingSchema(
  citizen: z.infer<typeof citizenBaseSchema>,
  citizenResponse,
  prefix: FieldPrefix,
) {
  const parsedCitizen = comparisonSchema.parse(citizen); // filters unwanted fields like userPreference
  const parsedCitizenResponse = comparisonSchema.parse(citizenResponse);

  // if data is in correct format aligned with comparisonSchema, take that data and compare values
  const changes = diff(parsedCitizen, parsedCitizenResponse);

  if (changes.length > 0) {
    const fieldErrors: FieldError[] = changes.map((diffFields) => ({
      field: prefix + "." + diffFields.path.join("."),
      message: "does not match",
    }));
    throw new BadRequest(
      RECORD_MISMATCH_VALIDATION_MESSAGE,
      new Date(),
      fieldErrors,
    );
  } else {
    return true;
  }
}

export function handleCitizenMatch(
  existingCitizen: CitizenGetResponse,
  citizen: CitizenBaseSchema | null,
  prefix: FieldPrefix,
) {
  if (!citizen) return null;
  if (!existingCitizen.id) {
    throw new BadRequest(RECORD_MISMATCH_VALIDATION_MESSAGE, new Date(), [
      {
        field: prefix + ".id",
        message: "is not stored in database",
      },
    ]);
  }
  loggerWithContext().info(
    `Matching citizen record for id:[${existingCitizen.id}]`,
  );

  if (compareCitizenObjectsUsingSchema(citizen, existingCitizen, prefix)) {
    return existingCitizen;
  }
}
