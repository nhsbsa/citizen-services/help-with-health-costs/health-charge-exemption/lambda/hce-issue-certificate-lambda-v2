import { createCitizenPromiseForId, getEvent } from "../common";
import { MandatoryHeaders } from "@nhsbsa/health-charge-exemption-npm-utils-rest";
import { mockCitizenResponse } from "../../../__mocks__/citizen/citizen-mocks";
import { mockUserPreferenceResponse } from "../../../__mocks__/user-preference/user-preference-mocks";
import {
  CITIZEN_BASE_URL_PATH,
  MOCK_CITIZEN_ID,
  USER_PREFERENCE_FIND_BY_CITIZEN_ID_AND_TYPE,
  mockMandatoryHeaders,
} from "../../../__tests__/utils/test-constants";
import {
  citizenApiSpyInstance,
  userPreferenceApiSpyInstance,
} from "../../../../test-setup/setup";
import { buildExpectedGetRequest } from "../../../__tests__/utils/test-functions";
import {
  CertificateType,
  Channel,
  Event,
  Preference,
} from "@nhsbsa/health-charge-exemption-npm-common-models";

beforeEach(() => {
  jest.clearAllMocks();
  citizenApiSpyInstance.mockResolvedValue(mockCitizenResponse);
  userPreferenceApiSpyInstance.mockResolvedValue(mockUserPreferenceResponse);
});

describe.each([
  CertificateType.HRT_PPC,
  CertificateType.LIS_HC2,
  CertificateType.LIS_HC3,
])("createCitizenPromiseForId", (certificateType: CertificateType) => {
  test("should return promises when id is provided", async () => {
    // given / when
    const promises = createCitizenPromiseForId(
      MOCK_CITIZEN_ID,
      certificateType,
      mockMandatoryHeaders,
    );

    // then
    expect(promises).toHaveLength(2);
    expect(citizenApiSpyInstance).toHaveBeenCalledTimes(1);
    expect(citizenApiSpyInstance).toHaveBeenCalledWith(
      buildExpectedGetRequest(
        CITIZEN_BASE_URL_PATH + "/" + MOCK_CITIZEN_ID,
        mockMandatoryHeaders,
      ),
    );
    expect(userPreferenceApiSpyInstance).toHaveBeenCalledTimes(1);
    expect(userPreferenceApiSpyInstance).toHaveBeenCalledWith({
      ...buildExpectedGetRequest(
        USER_PREFERENCE_FIND_BY_CITIZEN_ID_AND_TYPE,
        mockMandatoryHeaders,
      ),
      params: {
        certificateType: certificateType,
        citizenId: MOCK_CITIZEN_ID,
      },
    });

    const [citizenResult, userPreferenceResult] = await Promise.all(promises);
    expect(citizenResult).toEqual(mockCitizenResponse);
    expect(userPreferenceResult).toEqual(mockUserPreferenceResponse);
  });

  test("should return null promises when id is undefined", async () => {
    // given
    const id = undefined;

    // when
    const promises = createCitizenPromiseForId(
      id,
      certificateType,
      mockMandatoryHeaders,
    );

    // then
    expect(promises).toHaveLength(2);
    expect(citizenApiSpyInstance).not.toHaveBeenCalled();
    expect(userPreferenceApiSpyInstance).not.toHaveBeenCalled();

    const [citizenResult, userPreferenceResult] = await Promise.all(promises);
    expect(citizenResult).toBeNull();
    expect(userPreferenceResult).toBeNull();
  });
});

describe("getEvent", () => {
  test("should return ANY when channel is HRT_ONLINE and preference is EMAIL", () => {
    // given
    const citizen = { userPreference: { preference: Preference.EMAIL } };

    // when
    const result = getEvent(mockMandatoryHeaders, citizen);

    // then
    expect(result).toBe(Event.ANY);
  });

  test("should return REMINDER when channel is HRT_ONLINE and preference is POSTAL", () => {
    // given
    const citizen = { userPreference: { preference: Preference.POSTAL } };

    // when
    const result = getEvent(mockMandatoryHeaders, citizen);

    // then
    expect(result).toBe(Event.REMINDER);
  });

  test.each([
    Channel.BATCH,
    Channel.BLC_ONLINE,
    Channel.HRT_STAFF,
    Channel.LIS_STAFF,
    Channel.PHARMACY,
  ])(
    "should return ANY when channel is %s and preference is EMAIL",
    (channel: Channel) => {
      // given
      const headers = { channel: channel } as MandatoryHeaders;
      const citizen = { userPreference: { preference: Preference.EMAIL } };

      // when
      const result = getEvent(headers, citizen);

      // then
      expect(result).toBe(Event.ANY);
    },
  );

  test.each([
    Channel.BATCH,
    Channel.BLC_ONLINE,
    Channel.HRT_STAFF,
    Channel.LIS_STAFF,
    Channel.PHARMACY,
  ])(
    "should return ANY when channel is %s and preference is POSTAL",
    (channel: Channel) => {
      // given
      const headers = { channel: channel } as MandatoryHeaders;
      const citizen = { userPreference: { preference: Preference.POSTAL } };

      // when
      const result = getEvent(headers, citizen);

      // then
      expect(result).toBe(Event.ANY);
    },
  );
});
