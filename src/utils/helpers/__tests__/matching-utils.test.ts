import {
  compareCitizenObjectsUsingSchema,
  handleCitizenMatch,
} from "../matching-utils";
import { generateMock } from "@anatine/zod-mock";
import { citizenBaseSchema } from "../../../schemas";
import {
  mockCitizen,
  mockCitizenLisStaffFullResponse,
  mockCitizenResponse,
} from "../../../__mocks__/citizen/citizen-mocks";
import { winstonInfoLoggerSpy } from "../../../../test-setup/setup";
import {
  mockedDate,
  RECORD_MISMATCH_VALIDATION_MESSAGE,
  DOES_NOT_MATCH_VALIDATION_MESSAGE,
  NOT_IN_DATABASE_VALIDATION_MESSAGE,
  CITIZEN_FIELD_KEY,
  PARTNER_FIELD_KEY,
} from "../../../__tests__/utils/test-constants";
import { FieldPrefix } from "../common";
import {
  CitizenGetResponse,
  Preference,
} from "@nhsbsa/health-charge-exemption-npm-common-models";

const prefixes: FieldPrefix[] = [CITIZEN_FIELD_KEY, PARTNER_FIELD_KEY];

beforeEach(() => {
  jest.clearAllMocks();
  jest.useFakeTimers().setSystemTime(mockedDate);
});

describe.each(prefixes)(
  "compareCitizenObjectsUsingSchema",
  (prefix: FieldPrefix) => {
    it("should return true when the objects match ignoring userPreference in citizen", () => {
      // given
      const mockedCitizen = {
        firstName: "John",
        lastName: "Smith",
        dateOfBirth: "2022-11-18",
        nhsNumber: "5165713040",
        userPreference: {
          preference: Preference.EMAIL,
        },
      };

      // when
      const result = compareCitizenObjectsUsingSchema(
        mockedCitizen,
        mockCitizenResponse,
        prefix,
      );

      // then
      expect(result).toBe(true);
    });

    it("should return true when citizen response emails is empty array", () => {
      // given
      const mockedCitizen = {
        firstName: "John",
        lastName: "doe",
        userPreference: {
          preference: Preference.EMAIL,
        },
      };
      const citizenResponse = {
        firstName: "John",
        lastName: "doe",
        emails: [],
      };

      // when
      const result = compareCitizenObjectsUsingSchema(
        mockedCitizen,
        citizenResponse,
        prefix,
      );

      // then
      expect(result).toBe(true);
    });

    it("should throw bad request when a single field does not match", async () => {
      // given
      const mockedCitizen = {
        firstName: "Jane",
        lastName: "Smith",
        dateOfBirth: "2022-11-18",
        nhsNumber: "5165713040",
        userPreference: {
          preference: Preference.EMAIL,
        },
      };

      // when / then
      await expect(async () =>
        compareCitizenObjectsUsingSchema(
          mockedCitizen,
          mockCitizenResponse,
          prefix,
        ),
      ).rejects.toThrow(
        expect.objectContaining({
          message: RECORD_MISMATCH_VALIDATION_MESSAGE,
          timestamp: mockedDate,
          fieldErrors: [
            {
              field: prefix + ".firstName",
              message: DOES_NOT_MATCH_VALIDATION_MESSAGE,
            },
          ],
        }),
      );
    });

    it.each([
      { field: "addressLine1" },
      { field: "addressLine2" },
      { field: "townOrCity" },
      { field: "country" },
      { field: "postcode" },
    ])(
      "should throw bad request when nested address field %s does not match",
      async ({ field }) => {
        // given
        const mockedCitizen = {
          firstName: "John",
          lastName: "Smith",
          dateOfBirth: "2022-11-18",
          nhsNumber: "5165713040",
          addresses: [
            {
              addressLine1: "Stella House",
              addressLine2: "Goldcrest Way",
              townOrCity: "Newcastle upon Tyne",
              country: "England",
              postcode: "NE15 8NY",
            },
          ],
          userPreference: {
            preference: Preference.EMAIL,
          },
        };
        mockedCitizen.addresses[0][field] = "";

        // when / then
        await expect(async () =>
          compareCitizenObjectsUsingSchema(
            mockedCitizen,
            {
              ...mockCitizenResponse,
              addresses: [
                {
                  addressLine1: "Stella House",
                  addressLine2: "Goldcrest Way",
                  townOrCity: "Newcastle upon Tyne",
                  country: "England",
                  postcode: "NE15 8NY",
                },
              ],
            },
            prefix,
          ),
        ).rejects.toThrow(
          expect.objectContaining({
            message: RECORD_MISMATCH_VALIDATION_MESSAGE,
            timestamp: new Date(),
            fieldErrors: [
              {
                field: prefix + ".addresses.0." + field,
                message: DOES_NOT_MATCH_VALIDATION_MESSAGE,
              },
            ],
          }),
        );
      },
    );

    it("should throw bad request for all field errors", async () => {
      // given
      const mockedCitizen = generateMock(citizenBaseSchema);
      mockedCitizen.emails = [{ emailAddress: "test@bsa.uk" }];

      // when / then
      await expect(async () =>
        compareCitizenObjectsUsingSchema(
          mockedCitizen,
          mockCitizenResponse,
          prefix,
        ),
      ).rejects.toThrow(
        expect.objectContaining({
          message: RECORD_MISMATCH_VALIDATION_MESSAGE,
          timestamp: mockedDate,
          fieldErrors: [
            {
              field: prefix + ".emails.0",
              message: DOES_NOT_MATCH_VALIDATION_MESSAGE,
            },
            {
              field: prefix + ".addresses",
              message: DOES_NOT_MATCH_VALIDATION_MESSAGE,
            },
            {
              field: prefix + ".firstName",
              message: DOES_NOT_MATCH_VALIDATION_MESSAGE,
            },
            {
              field: prefix + ".lastName",
              message: DOES_NOT_MATCH_VALIDATION_MESSAGE,
            },
            {
              field: prefix + ".dateOfBirth",
              message: DOES_NOT_MATCH_VALIDATION_MESSAGE,
            },
            {
              field: prefix + ".nhsNumber",
              message: DOES_NOT_MATCH_VALIDATION_MESSAGE,
            },
          ],
        }),
      );
    });

    it.each([
      { field: "firstName" },
      { field: "lastName" },
      { field: "dateOfBirth" },
      { field: "nhsNumber" },
      { field: "emails", value: [] },
      { field: "addresses", value: [] },
    ])(
      "should throw bad request when mismatched field error for %s",
      async ({ field, value }) => {
        // given
        const mockedCitizen = {
          ...mockCitizen,
          userPreference: {
            // user preference included to be test ignorance
            preference: Preference.EMAIL,
          },
        };
        mockedCitizen[field] = value ?? "";

        // when / then
        let expectedFieldError = prefix + "." + field;
        if (value) {
          expectedFieldError = expectedFieldError + ".0";
        }
        await expect(async () =>
          compareCitizenObjectsUsingSchema(
            mockedCitizen,
            mockCitizenLisStaffFullResponse,
            prefix,
          ),
        ).rejects.toThrow(
          expect.objectContaining({
            message: RECORD_MISMATCH_VALIDATION_MESSAGE,
            timestamp: mockedDate,
            fieldErrors: [
              {
                field: expectedFieldError,
                message: DOES_NOT_MATCH_VALIDATION_MESSAGE,
              },
            ],
          }),
        );
      },
    );
  },
);

describe.each(prefixes)("handleCitizenMatch", (prefix: FieldPrefix) => {
  const existingCitizen = {
    firstName: "john",
    lastName: "doe",
  } as unknown as CitizenGetResponse;

  it("should return null if citizen is null", () => {
    // given / when
    const result = handleCitizenMatch(existingCitizen, null, prefix);

    // then
    expect(result).toBeNull();
  });

  test("should throw BadRequest if existingCitizen.id is not defined", async () => {
    // given
    const mockedCitizen = generateMock(citizenBaseSchema);

    // when
    await expect(async () =>
      handleCitizenMatch(existingCitizen, mockedCitizen, prefix),
    ).rejects.toThrow(
      expect.objectContaining({
        message: RECORD_MISMATCH_VALIDATION_MESSAGE,
        timestamp: mockedDate,
        fieldErrors: [
          {
            field: prefix + ".id",
            message: NOT_IN_DATABASE_VALIDATION_MESSAGE,
          },
        ],
      }),
    );
  });

  test("should log info message with existingCitizen.id", () => {
    // given
    existingCitizen.id = "123";
    const mockedCitizen = {
      ...existingCitizen,
      userPreference: {
        preference: Preference.EMAIL,
      },
    };

    // when
    handleCitizenMatch(existingCitizen, mockedCitizen, prefix);

    // then
    expect(winstonInfoLoggerSpy).toHaveBeenCalledWith(
      "Matching citizen record for id:[123]",
    );
  });

  test("should return existingCitizen if citizen matches existingCitizen", () => {
    // given
    existingCitizen.id = "123";
    const mockedCitizen = {
      ...existingCitizen,
      userPreference: {
        preference: Preference.EMAIL,
      },
    };

    // when
    const result = handleCitizenMatch(existingCitizen, mockedCitizen, prefix);

    // then
    expect(result).toEqual(existingCitizen);
  });

  test("should return bad request if citizen does not match existingCitizen", async () => {
    // given
    existingCitizen.id = "123";
    const mockedCitizen = {
      ...existingCitizen,
      firstName: "jane",
      userPreference: {
        preference: Preference.EMAIL,
      },
    };

    // when / then
    await expect(async () =>
      handleCitizenMatch(existingCitizen, mockedCitizen, prefix),
    ).rejects.toThrow(
      expect.objectContaining({
        message: RECORD_MISMATCH_VALIDATION_MESSAGE,
        timestamp: mockedDate,
        fieldErrors: [
          {
            field: prefix + ".firstName",
            message: DOES_NOT_MATCH_VALIDATION_MESSAGE,
          },
        ],
      }),
    );
  });
});
