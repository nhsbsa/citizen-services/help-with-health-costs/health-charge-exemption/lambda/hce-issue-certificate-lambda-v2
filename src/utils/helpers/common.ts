import { MandatoryHeaders } from "@nhsbsa/health-charge-exemption-npm-utils-rest";
import { getCitizenById } from "../api/citizen";
import {
  createUserPreferencePatchData,
  createUserPreferencePostData,
  findUserPreferenceForCitizen,
  patchUserPreference,
  persistUserPreference,
} from "../api/user-preference";
import {
  CertificateSchema,
  CitizenBaseSchema,
  citizenBaseResponseSchema,
} from "../../schemas";
import { loggerWithContext } from "@nhsbsa/health-charge-exemption-npm-logging";
import {
  Channel,
  Event,
  Preference,
  UserPreferenceGetResponse,
  UserPreferencePostResponse,
  UserPreferencePatchResponse,
  CitizenGetResponse,
} from "@nhsbsa/health-charge-exemption-npm-common-models";

export const CITIZEN_FIELD_KEY = "citizen";
export const PARTNER_FIELD_KEY = "partner";

export type FieldPrefix = typeof CITIZEN_FIELD_KEY | typeof PARTNER_FIELD_KEY;
export type CitizenUserPreferenceResponse =
  | CitizenGetResponse
  | UserPreferenceGetResponse
  | null;
export type OptionalCitizenResponse = CitizenGetResponse | null | undefined;

export function getEvent(
  headers: MandatoryHeaders,
  citizen: CitizenBaseSchema,
) {
  // currently ANY is used for LIS types as no requirement for REMINDER
  return headers.channel === Channel.HRT_ONLINE &&
    citizen.userPreference.preference === Preference.POSTAL
    ? Event.REMINDER
    : Event.ANY;
}

export function createCitizenPromiseForId(
  id: string | undefined,
  certificateType: string | undefined,
  headers: MandatoryHeaders,
) {
  const promises: Promise<CitizenUserPreferenceResponse>[] = [];
  if (id) {
    promises.push(getCitizenById(id, headers));
    promises.push(findUserPreferenceForCitizen(certificateType, id, headers));
  } else {
    promises.push(Promise.resolve(null));
    promises.push(Promise.resolve(null));
  }
  return promises;
}

export function parseUserPreferenceForCitizenResponse(
  citizenResponse: OptionalCitizenResponse,
  userPreference: UserPreferencePostResponse | UserPreferencePatchResponse,
  existingUserPreference: CitizenUserPreferenceResponse,
) {
  return citizenBaseResponseSchema.parse({
    ...citizenResponse,
    userPreference: userPreference ?? existingUserPreference,
  });
}

export async function createUserPreferencePromise(
  citizenResponse: OptionalCitizenResponse,
  citizen: CitizenBaseSchema,
  existingPreference: CitizenUserPreferenceResponse,
  certificate: CertificateSchema,
  headers: MandatoryHeaders,
) {
  if (!existingPreference?.id && citizenResponse) {
    return persistUserPreference(
      createUserPreferencePostData(certificate, citizen, headers),
      citizenResponse.id,
      headers,
    ).then((response: UserPreferencePostResponse) => {
      loggerWithContext().info(
        `User Preference record saved successfully, userPreferenceId:[${response.id}]`,
      );
      return response;
    });
  } else if (
    existingPreference &&
    (existingPreference as UserPreferenceGetResponse).preference !==
      citizen.userPreference.preference
  ) {
    return patchUserPreference(
      createUserPreferencePatchData(citizen, headers),
      existingPreference.id,
      headers,
    ).then((response: UserPreferencePatchResponse) => {
      loggerWithContext().info(
        `User Preference record updated successfully ${response.id}`,
      );
      return response;
    });
  }
  return Promise.resolve(null);
}
