export { default as mockUserPreferenceResponse } from "./response/user-preference-api-response.json";
export { default as mockUserPreferencePartnerResponse } from "./response/user-preference-partner-api-response.json";
