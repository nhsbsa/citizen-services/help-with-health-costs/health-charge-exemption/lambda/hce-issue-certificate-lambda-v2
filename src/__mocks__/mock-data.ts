export { default as mockSuccessResponse } from "./success-response.json";
export { default as mockSuccessResponseWithPartner } from "./success-response-with-partner.json";
export { default as mockSuccessResponseWithPartnerPatchedPreference } from "./success-response-with-partner-preference-patch.json";
export { default as mockSuccessResponseLisHc2 } from "./success-response-lishc2.json";
export { default as mockSuccessResponseForNewCitizenButPartnerMatch } from "./success-response-new-citizen-matching-partner.json";
export { default as mockHeaders } from "./headers.json";
