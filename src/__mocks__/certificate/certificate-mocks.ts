export { default as mockCertificate } from "./get/request/certificate-api-request.json";
export { default as mockLisCertificate } from "./get/request/certificate-lis-api-request.json";
export { default as mockCertificateResponse } from "./get/response/certificate-api-response.json";
export { default as mockLisCertificateResponse } from "./get/response/certificate-lis-api-response.json";
