export { default as mockCitizen } from "./get/request/citizen-api-request.json";
export { default as mockPartnerCitizen } from "./get/request/citizen-partner-api-request.json";
export { default as mockCitizenLisStaffFullResponse } from "./get/response/citizen-api-full-response-lis-staff.json";
export { default as mockCitizenHrtOnlineFullResponse } from "./get/response/citizen-api-full-response-hrt-online.json";
export { default as mockPartnerFullResponse } from "./get/response/citizen-api-full-partner-response.json";
export { default as mockCitizenResponse } from "./post/response/citizen-api-response.json";
export { default as mockPartnerCitizenResponse } from "./post/response/citizen-partner-api-response.json";
