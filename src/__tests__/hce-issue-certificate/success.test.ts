import {
  citizenApiSpyInstance,
  certificateApiSpyInstance,
  userPreferenceApiSpyInstance,
  winstonInfoLoggerSpy,
} from "../../../test-setup/setup";
import {
  mockCertificateResponse,
  mockCertificate,
  mockLisCertificateResponse,
  mockLisCertificate,
} from "../../__mocks__/certificate/certificate-mocks";
import {
  mockCitizenResponse,
  mockPartnerCitizenResponse,
  mockCitizenLisStaffFullResponse,
  mockPartnerFullResponse,
  mockCitizenHrtOnlineFullResponse,
} from "../../__mocks__/citizen/citizen-mocks";
import {
  mockSuccessResponse,
  mockSuccessResponseWithPartner,
  mockSuccessResponseLisHc2,
  mockSuccessResponseWithPartnerPatchedPreference,
  mockSuccessResponseForNewCitizenButPartnerMatch,
} from "../../__mocks__/mock-data";
import {
  mockUserPreferenceResponse,
  mockUserPreferencePartnerResponse,
} from "../../__mocks__/user-preference/user-preference-mocks";
import { issueCertificate } from "../../hce-issue-certificate";
import {
  CERTIFICATE_BASE_URL_PATH,
  CITIZEN_BASE_URL_PATH,
  MOCK_CITIZEN_ID,
  MOCK_CITIZEN_PARTNER_ID,
  SERVICE_ENTERED_LOG_MESSAGE,
  USER_PREFERENCE_BASE_URL_PATH,
  USER_PREFERENCE_FIND_BY_CITIZEN_ID_AND_TYPE,
  citizenBaseSchemaMock,
  mockCitizenInRequestBodyWithId,
  mockMandatoryHeaders,
  mockMandatoryLisHeaders,
  mockPartnerInRequestBodyWithId,
  partnerBaseSchemaMock,
  mockPatchUserPreferenceRequest,
  MOCK_USER_PREFERENCE_ID,
  mockPostUserPreferenceRequest,
  CITIZEN_FIELD_KEY,
} from "../utils/test-constants";
import {
  buildExpectedGetRequest,
  buildExpectedPatchRequest,
  buildExpectedPostRequest,
} from "../utils/test-functions";
import {
  CertificateType,
  Channel,
  Event,
  Preference,
} from "@nhsbsa/health-charge-exemption-npm-common-models";

afterEach(() => {
  expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
    1,
    SERVICE_ENTERED_LOG_MESSAGE,
  );
});

describe("issueCertificate - citizen with no partner", () => {
  beforeEach(() => {
    certificateApiSpyInstance.mockResolvedValue(mockCertificateResponse);
    userPreferenceApiSpyInstance.mockResolvedValue(mockUserPreferenceResponse);
  });

  afterEach(() => {
    expect(citizenApiSpyInstance).toHaveBeenCalledTimes(1);
    expect(certificateApiSpyInstance).toHaveBeenCalledTimes(1);
  });

  it("should respond with the success response when valid citizen and certificate", async () => {
    // given
    citizenApiSpyInstance.mockResolvedValue(mockCitizenResponse);

    // when
    const result = await issueCertificate(
      mockMandatoryHeaders,
      citizenBaseSchemaMock,
      null,
      mockCertificate,
    );

    // then
    expect(result).toEqual(mockSuccessResponse);
    expect(citizenApiSpyInstance).toHaveBeenNthCalledWith(
      1,
      buildExpectedPostRequest(
        CITIZEN_BASE_URL_PATH,
        { ...citizenBaseSchemaMock },
        mockMandatoryHeaders,
      ),
    );
    expect(certificateApiSpyInstance).toHaveBeenNthCalledWith(1, {
      ...buildExpectedPostRequest(
        CERTIFICATE_BASE_URL_PATH,
        mockCertificate,
        mockMandatoryHeaders,
      ),
      params: { citizenId: MOCK_CITIZEN_ID },
    });
    expect(userPreferenceApiSpyInstance).toHaveBeenCalledTimes(1);
    expect(userPreferenceApiSpyInstance).toHaveBeenNthCalledWith(1, {
      ...buildExpectedPostRequest(
        USER_PREFERENCE_BASE_URL_PATH,
        {
          ...mockPostUserPreferenceRequest,
          certificateType: CertificateType.HRT_PPC,
        },
        mockMandatoryHeaders,
      ),
      params: { citizenId: MOCK_CITIZEN_ID },
    });
    expect(winstonInfoLoggerSpy).toHaveBeenCalledTimes(4);
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      2,
      `Citizen record saved successfully, citizenId:[${MOCK_CITIZEN_ID}]`,
    );
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      3,
      "Certificate record saved successfully, certificateId:[ac1be9c0-8578-15b0-8185-7da2d6a00010, reference: [3569BDF217A46923DB1A]]",
    );
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      4,
      "User Preference record saved successfully, userPreferenceId:[c0a800f6-8806-16e9-8188-062879e40000]",
    );
  });

  it.each(["12345678", "00000001", "12345678901234567890"])(
    "should respond with the success response when certificate reference is %s",
    async (reference: string) => {
      // given
      citizenApiSpyInstance.mockResolvedValue(mockCitizenResponse);
      certificateApiSpyInstance.mockResolvedValue({
        ...mockCertificateResponse,
        reference: reference,
      });
      const mockCertificateWithReference = {
        ...mockCertificate,
        reference: reference,
      };

      // when
      const result = await issueCertificate(
        mockMandatoryHeaders,
        citizenBaseSchemaMock,
        null,
        mockCertificateWithReference,
      );

      // then
      expect(result).toEqual({
        citizen: mockSuccessResponse[CITIZEN_FIELD_KEY],
        certificate: {
          ...mockSuccessResponse["certificate"],
          reference: reference,
        },
      });
      expect(citizenApiSpyInstance).toHaveBeenNthCalledWith(
        1,
        buildExpectedPostRequest(
          CITIZEN_BASE_URL_PATH,
          { ...citizenBaseSchemaMock },
          mockMandatoryHeaders,
        ),
      );
      expect(certificateApiSpyInstance).toHaveBeenNthCalledWith(1, {
        ...buildExpectedPostRequest(
          CERTIFICATE_BASE_URL_PATH,
          mockCertificateWithReference,
          mockMandatoryHeaders,
        ),
        params: { citizenId: MOCK_CITIZEN_ID },
      });
      expect(userPreferenceApiSpyInstance).toHaveBeenCalledTimes(1);
      expect(userPreferenceApiSpyInstance).toHaveBeenNthCalledWith(1, {
        ...buildExpectedPostRequest(
          USER_PREFERENCE_BASE_URL_PATH,
          {
            ...mockPostUserPreferenceRequest,
            certificateType: CertificateType.HRT_PPC,
          },
          mockMandatoryHeaders,
        ),
        params: { citizenId: MOCK_CITIZEN_ID },
      });
      expect(winstonInfoLoggerSpy).toHaveBeenCalledTimes(4);
      expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
        2,
        `Citizen record saved successfully, citizenId:[${MOCK_CITIZEN_ID}]`,
      );
      expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
        3,
        `Certificate record saved successfully, certificateId:[ac1be9c0-8578-15b0-8185-7da2d6a00010, reference: [${reference}]]`,
      );
      expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
        4,
        "User Preference record saved successfully, userPreferenceId:[c0a800f6-8806-16e9-8188-062879e40000]",
      );
    },
  );

  it("should return existing citizen when fields match", async () => {
    // given
    citizenApiSpyInstance.mockResolvedValue(mockCitizenHrtOnlineFullResponse);

    // when
    const result = await issueCertificate(
      mockMandatoryHeaders,
      mockCitizenInRequestBodyWithId,
      null,
      mockCertificate,
    );

    // then
    expect(result).toEqual(mockSuccessResponse);
    expect(citizenApiSpyInstance).toHaveBeenNthCalledWith(
      1,
      buildExpectedGetRequest(
        CITIZEN_BASE_URL_PATH + "/" + MOCK_CITIZEN_ID,
        mockMandatoryHeaders,
      ),
    );
    expect(certificateApiSpyInstance).toHaveBeenNthCalledWith(1, {
      ...buildExpectedPostRequest(
        CERTIFICATE_BASE_URL_PATH,
        mockCertificate,
        mockMandatoryHeaders,
      ),
      params: { citizenId: MOCK_CITIZEN_ID },
    });
    expect(userPreferenceApiSpyInstance).toHaveBeenCalledTimes(1);
    expect(userPreferenceApiSpyInstance).toHaveBeenNthCalledWith(1, {
      ...buildExpectedGetRequest(
        USER_PREFERENCE_FIND_BY_CITIZEN_ID_AND_TYPE,
        mockMandatoryHeaders,
      ),
      params: {
        certificateType: CertificateType.HRT_PPC,
        citizenId: MOCK_CITIZEN_ID,
      },
    });
    expect(winstonInfoLoggerSpy).toHaveBeenCalledTimes(3);
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      2,
      `Matching citizen record for id:[${MOCK_CITIZEN_ID}]`,
    );
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      3,
      "Certificate record saved successfully, certificateId:[ac1be9c0-8578-15b0-8185-7da2d6a00010, reference: [3569BDF217A46923DB1A]]",
    );
  });

  it("should return existing citizen when fields and id match", async () => {
    // given
    citizenApiSpyInstance.mockResolvedValue(mockCitizenHrtOnlineFullResponse);

    // when
    const result = await issueCertificate(
      mockMandatoryHeaders,
      mockCitizenInRequestBodyWithId,
      null,
      mockCertificate,
    );

    // then
    expect(result).toEqual(mockSuccessResponse);
    expect(citizenApiSpyInstance).toHaveBeenNthCalledWith(
      1,
      buildExpectedGetRequest(
        CITIZEN_BASE_URL_PATH + "/" + MOCK_CITIZEN_ID,
        mockMandatoryHeaders,
      ),
    );
    expect(certificateApiSpyInstance).toHaveBeenNthCalledWith(1, {
      ...buildExpectedPostRequest(
        CERTIFICATE_BASE_URL_PATH,
        mockCertificate,
        mockMandatoryHeaders,
      ),
      params: { citizenId: MOCK_CITIZEN_ID },
    });
    expect(userPreferenceApiSpyInstance).toHaveBeenCalledTimes(1);
    expect(userPreferenceApiSpyInstance).toHaveBeenNthCalledWith(1, {
      ...buildExpectedGetRequest(
        USER_PREFERENCE_FIND_BY_CITIZEN_ID_AND_TYPE,
        mockMandatoryHeaders,
      ),
      params: {
        certificateType: CertificateType.HRT_PPC,
        citizenId: MOCK_CITIZEN_ID,
      },
    });
    expect(winstonInfoLoggerSpy).toHaveBeenCalledTimes(3);
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      2,
      `Matching citizen record for id:[${MOCK_CITIZEN_ID}]`,
    );
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      3,
      "Certificate record saved successfully, certificateId:[ac1be9c0-8578-15b0-8185-7da2d6a00010, reference: [3569BDF217A46923DB1A]]",
    );
  });

  it("should return existing citizen and patch user preference when id provided but userPreference differs", async () => {
    // given
    citizenApiSpyInstance.mockResolvedValue(mockCitizenHrtOnlineFullResponse);
    userPreferenceApiSpyInstance.mockResolvedValue({
      ...mockUserPreferenceResponse,
      preference: Preference.POSTAL,
    });

    // when
    const result = await issueCertificate(
      mockMandatoryHeaders,
      mockCitizenInRequestBodyWithId,
      null,
      mockCertificate,
    );

    // then
    expect(result).toEqual({
      citizen: {
        ...mockSuccessResponse["citizen"],
        userPreference: {
          ...mockSuccessResponse["citizen"].userPreference,
          preference: Preference.POSTAL,
        },
      },
      certificate: mockSuccessResponse["certificate"],
    });
    expect(citizenApiSpyInstance).toHaveBeenNthCalledWith(
      1,
      buildExpectedGetRequest(
        CITIZEN_BASE_URL_PATH + "/" + MOCK_CITIZEN_ID,
        mockMandatoryHeaders,
      ),
    );
    expect(certificateApiSpyInstance).toHaveBeenNthCalledWith(1, {
      ...buildExpectedPostRequest(
        CERTIFICATE_BASE_URL_PATH,
        mockCertificate,
        mockMandatoryHeaders,
      ),
      params: { citizenId: MOCK_CITIZEN_ID },
    });
    expect(userPreferenceApiSpyInstance).toHaveBeenCalledTimes(2);
    expect(userPreferenceApiSpyInstance).toHaveBeenNthCalledWith(1, {
      ...buildExpectedGetRequest(
        USER_PREFERENCE_FIND_BY_CITIZEN_ID_AND_TYPE,
        mockMandatoryHeaders,
      ),
      params: {
        certificateType: CertificateType.HRT_PPC,
        citizenId: MOCK_CITIZEN_ID,
      },
    });
    expect(userPreferenceApiSpyInstance).toHaveBeenNthCalledWith(
      2,
      buildExpectedPatchRequest(
        USER_PREFERENCE_BASE_URL_PATH + "/c0a800f6-8806-16e9-8188-062879e40000",
        mockPatchUserPreferenceRequest,
        mockMandatoryHeaders,
      ),
    );
    expect(winstonInfoLoggerSpy).toHaveBeenCalledTimes(4);
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      2,
      `Matching citizen record for id:[${MOCK_CITIZEN_ID}]`,
    );
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      3,
      "Certificate record saved successfully, certificateId:[ac1be9c0-8578-15b0-8185-7da2d6a00010, reference: [3569BDF217A46923DB1A]]",
    );
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      4,
      "User Preference record updated successfully c0a800f6-8806-16e9-8188-062879e40000",
    );
  });
});

describe("issueCertificate - citizen and partner", () => {
  // setup
  const userPreferenceGetResponseWithLisStaffChannel = {
    ...mockUserPreferenceResponse,
    certificateType: CertificateType.LIS_HC2,
    _meta: {
      ...mockUserPreferenceResponse._meta,
      channel: Channel.LIS_STAFF,
    },
  };

  it("should respond with the success response when valid citizen, certificate and partner", async () => {
    // given
    const mockCitizenResponseWithLisStaffChannel = {
      ...mockCitizenResponse,
      _meta: { ...mockCitizenResponse._meta, channel: Channel.LIS_STAFF },
    };
    citizenApiSpyInstance
      .mockImplementationOnce(() =>
        Promise.resolve(mockCitizenResponseWithLisStaffChannel),
      )
      .mockImplementationOnce(() =>
        Promise.resolve(mockPartnerCitizenResponse),
      );

    certificateApiSpyInstance.mockResolvedValue(mockLisCertificateResponse);
    userPreferenceApiSpyInstance
      .mockImplementationOnce(() =>
        Promise.resolve(userPreferenceGetResponseWithLisStaffChannel),
      )
      .mockImplementationOnce(() =>
        Promise.resolve(mockUserPreferencePartnerResponse),
      );

    // when
    const result = await issueCertificate(
      mockMandatoryLisHeaders,
      citizenBaseSchemaMock,
      partnerBaseSchemaMock,
      mockLisCertificate,
    );

    // then
    expect(result).toEqual(mockSuccessResponseWithPartner);
    expect(citizenApiSpyInstance).toHaveBeenCalledTimes(2);
    expect(citizenApiSpyInstance).toHaveBeenNthCalledWith(
      1,
      buildExpectedPostRequest(
        CITIZEN_BASE_URL_PATH,
        { ...citizenBaseSchemaMock },
        mockMandatoryLisHeaders,
      ),
    );
    expect(citizenApiSpyInstance).toHaveBeenNthCalledWith(
      2,
      buildExpectedPostRequest(
        CITIZEN_BASE_URL_PATH,
        { ...partnerBaseSchemaMock },
        mockMandatoryLisHeaders,
      ),
    );
    expect(certificateApiSpyInstance).toHaveBeenCalledTimes(1);
    expect(certificateApiSpyInstance).toHaveBeenNthCalledWith(1, {
      ...buildExpectedPostRequest(
        CERTIFICATE_BASE_URL_PATH,
        {
          ...mockLisCertificate,
          lowIncomeScheme: {
            partnerCitizenId: MOCK_CITIZEN_PARTNER_ID,
          },
        },
        mockMandatoryLisHeaders,
      ),
      params: { citizenId: MOCK_CITIZEN_ID },
    });
    expect(userPreferenceApiSpyInstance).toHaveBeenCalledTimes(2);
    expect(userPreferenceApiSpyInstance).toHaveBeenNthCalledWith(1, {
      ...buildExpectedPostRequest(
        USER_PREFERENCE_BASE_URL_PATH,
        mockPostUserPreferenceRequest,
        mockMandatoryLisHeaders,
      ),
      params: { citizenId: MOCK_CITIZEN_ID },
    });
    expect(userPreferenceApiSpyInstance).toHaveBeenNthCalledWith(2, {
      ...buildExpectedPostRequest(
        USER_PREFERENCE_BASE_URL_PATH,
        mockPostUserPreferenceRequest,
        mockMandatoryLisHeaders,
      ),
      params: { citizenId: MOCK_CITIZEN_PARTNER_ID },
    });
    expect(winstonInfoLoggerSpy).toHaveBeenCalledTimes(6);
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      2,
      `Citizen record saved successfully, citizenId:[${MOCK_CITIZEN_ID}]`,
    );
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      3,
      `Citizen record saved for partner successfully, citizenId:[${MOCK_CITIZEN_PARTNER_ID}]`,
    );
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      4,
      "Certificate record saved successfully, certificateId:[ac1be9c0-8578-15b0-8185-7da2d6a00010, reference: [12345678]]",
    );
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      5,
      "User Preference record saved successfully, userPreferenceId:[c0a800f6-8806-16e9-8188-062879e40000]",
    );
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      6,
      "User Preference record saved successfully, userPreferenceId:[preferen-cexx-idxx-xxxx-xxxxxxxxxxxx]",
    );
  });

  it("should respond with the success response when matching citizen and partner", async () => {
    // given
    citizenApiSpyInstance
      .mockImplementationOnce(() =>
        Promise.resolve(mockCitizenLisStaffFullResponse),
      )
      .mockImplementationOnce(() => Promise.resolve(mockPartnerFullResponse));
    certificateApiSpyInstance.mockResolvedValue(mockLisCertificateResponse);
    userPreferenceApiSpyInstance
      .mockImplementationOnce(() =>
        Promise.resolve(userPreferenceGetResponseWithLisStaffChannel),
      )
      .mockImplementationOnce(() =>
        Promise.resolve(mockUserPreferencePartnerResponse),
      );

    // when
    const result = await issueCertificate(
      mockMandatoryLisHeaders,
      mockCitizenInRequestBodyWithId,
      mockPartnerInRequestBodyWithId,
      mockLisCertificate,
    );

    // then
    expect(result).toEqual(mockSuccessResponseWithPartner);
    expect(citizenApiSpyInstance).toHaveBeenCalledTimes(2);
    expect(citizenApiSpyInstance).toHaveBeenNthCalledWith(1, {
      ...buildExpectedGetRequest(
        CITIZEN_BASE_URL_PATH + "/" + MOCK_CITIZEN_ID,
        mockMandatoryLisHeaders,
      ),
    });
    expect(citizenApiSpyInstance).toHaveBeenNthCalledWith(
      2,
      buildExpectedGetRequest(
        CITIZEN_BASE_URL_PATH + "/" + MOCK_CITIZEN_PARTNER_ID,
        mockMandatoryLisHeaders,
      ),
    );
    expect(certificateApiSpyInstance).toHaveBeenCalledTimes(1);
    expect(certificateApiSpyInstance).toHaveBeenNthCalledWith(1, {
      ...buildExpectedPostRequest(
        CERTIFICATE_BASE_URL_PATH,
        {
          ...mockLisCertificate,
          lowIncomeScheme: {
            partnerCitizenId: MOCK_CITIZEN_PARTNER_ID,
          },
        },
        mockMandatoryLisHeaders,
      ),
      params: { citizenId: MOCK_CITIZEN_ID },
    });
    expect(userPreferenceApiSpyInstance).toHaveBeenCalledTimes(2);
    expect(userPreferenceApiSpyInstance).toHaveBeenNthCalledWith(1, {
      ...buildExpectedGetRequest(
        USER_PREFERENCE_FIND_BY_CITIZEN_ID_AND_TYPE,
        mockMandatoryLisHeaders,
      ),
      params: {
        certificateType: CertificateType.LIS_HC2,
        citizenId: MOCK_CITIZEN_ID,
      },
    });
    expect(userPreferenceApiSpyInstance).toHaveBeenNthCalledWith(2, {
      ...buildExpectedGetRequest(
        USER_PREFERENCE_FIND_BY_CITIZEN_ID_AND_TYPE,
        mockMandatoryLisHeaders,
      ),
      params: {
        certificateType: CertificateType.LIS_HC2,
        citizenId: MOCK_CITIZEN_PARTNER_ID,
      },
    });
    expect(winstonInfoLoggerSpy).toHaveBeenCalledTimes(4);
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      2,
      `Matching citizen record for id:[${MOCK_CITIZEN_ID}]`,
    );
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      3,
      `Matching citizen record for id:[${MOCK_CITIZEN_PARTNER_ID}]`,
    );
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      4,
      "Certificate record saved successfully, certificateId:[ac1be9c0-8578-15b0-8185-7da2d6a00010, reference: [12345678]]",
    );
  });

  it("should return citizen with new certificate and partner when existing citizen matches", async () => {
    // given
    citizenApiSpyInstance
      .mockImplementationOnce(() =>
        Promise.resolve(mockCitizenLisStaffFullResponse),
      )
      .mockImplementationOnce(() =>
        Promise.resolve(mockPartnerCitizenResponse),
      );

    certificateApiSpyInstance.mockResolvedValue(mockLisCertificateResponse);
    userPreferenceApiSpyInstance
      .mockImplementationOnce(() =>
        Promise.resolve(userPreferenceGetResponseWithLisStaffChannel),
      )
      .mockImplementationOnce(() =>
        Promise.resolve(mockUserPreferencePartnerResponse),
      );

    // when
    const result = await issueCertificate(
      mockMandatoryLisHeaders,
      mockCitizenInRequestBodyWithId,
      partnerBaseSchemaMock,
      mockLisCertificate,
    );

    // then
    expect(result).toEqual(mockSuccessResponseWithPartner);
    expect(citizenApiSpyInstance).toHaveBeenCalledTimes(2);
    expect(citizenApiSpyInstance).toHaveBeenNthCalledWith(1, {
      ...buildExpectedGetRequest(
        CITIZEN_BASE_URL_PATH + "/" + MOCK_CITIZEN_ID,
        mockMandatoryLisHeaders,
      ),
    });
    expect(citizenApiSpyInstance).toHaveBeenNthCalledWith(
      2,
      buildExpectedPostRequest(
        CITIZEN_BASE_URL_PATH,
        partnerBaseSchemaMock,
        mockMandatoryLisHeaders,
      ),
    );
    expect(certificateApiSpyInstance).toHaveBeenCalledTimes(1);
    expect(certificateApiSpyInstance).toHaveBeenNthCalledWith(1, {
      ...buildExpectedPostRequest(
        CERTIFICATE_BASE_URL_PATH,
        {
          ...mockLisCertificate,
          lowIncomeScheme: {
            partnerCitizenId: MOCK_CITIZEN_PARTNER_ID,
          },
        },
        mockMandatoryLisHeaders,
      ),
      params: { citizenId: MOCK_CITIZEN_ID },
    });
    expect(userPreferenceApiSpyInstance).toHaveBeenCalledTimes(2);
    expect(userPreferenceApiSpyInstance).toHaveBeenNthCalledWith(1, {
      ...buildExpectedGetRequest(
        USER_PREFERENCE_FIND_BY_CITIZEN_ID_AND_TYPE,
        mockMandatoryLisHeaders,
      ),
      params: {
        certificateType: CertificateType.LIS_HC2,
        citizenId: MOCK_CITIZEN_ID,
      },
    });
    expect(userPreferenceApiSpyInstance).toHaveBeenNthCalledWith(2, {
      ...buildExpectedPostRequest(
        USER_PREFERENCE_BASE_URL_PATH,
        mockPostUserPreferenceRequest,
        mockMandatoryLisHeaders,
      ),
      params: {
        citizenId: MOCK_CITIZEN_PARTNER_ID,
      },
    });
    expect(winstonInfoLoggerSpy).toHaveBeenCalledTimes(5);
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      2,
      `Matching citizen record for id:[${MOCK_CITIZEN_ID}]`,
    );
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      3,
      `Citizen record saved for partner successfully, citizenId:[${MOCK_CITIZEN_PARTNER_ID}]`,
    );
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      4,
      "Certificate record saved successfully, certificateId:[ac1be9c0-8578-15b0-8185-7da2d6a00010, reference: [12345678]]",
    );
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      5,
      "User Preference record saved successfully, userPreferenceId:[preferen-cexx-idxx-xxxx-xxxxxxxxxxxx]",
    );
  });

  it("should return citizen with new certificate and partner when existing partner matches but new citizen", async () => {
    // given
    citizenApiSpyInstance
      .mockImplementationOnce(() => Promise.resolve(mockPartnerFullResponse))
      .mockImplementationOnce(() =>
        Promise.resolve({
          ...mockCitizenResponse,
          _meta: {
            ...mockCitizenResponse._meta,
            channel: Channel.LIS_STAFF,
          },
        }),
      );
    certificateApiSpyInstance.mockResolvedValue(mockLisCertificateResponse);
    userPreferenceApiSpyInstance
      .mockImplementationOnce(() =>
        Promise.resolve(mockUserPreferencePartnerResponse),
      )
      .mockImplementationOnce(() =>
        Promise.resolve(userPreferenceGetResponseWithLisStaffChannel),
      );

    // when
    const result = await issueCertificate(
      mockMandatoryLisHeaders,
      citizenBaseSchemaMock,
      mockPartnerInRequestBodyWithId,
      mockLisCertificate,
    );

    // then
    expect(result).toEqual(mockSuccessResponseForNewCitizenButPartnerMatch);
    expect(citizenApiSpyInstance).toHaveBeenCalledTimes(2);
    expect(citizenApiSpyInstance).toHaveBeenNthCalledWith(1, {
      ...buildExpectedGetRequest(
        CITIZEN_BASE_URL_PATH + "/" + MOCK_CITIZEN_PARTNER_ID,
        mockMandatoryLisHeaders,
      ),
    });
    expect(citizenApiSpyInstance).toHaveBeenNthCalledWith(
      2,
      buildExpectedPostRequest(
        CITIZEN_BASE_URL_PATH,
        citizenBaseSchemaMock,
        mockMandatoryLisHeaders,
      ),
    );
    expect(certificateApiSpyInstance).toHaveBeenCalledTimes(1);
    expect(certificateApiSpyInstance).toHaveBeenNthCalledWith(1, {
      ...buildExpectedPostRequest(
        CERTIFICATE_BASE_URL_PATH,
        {
          ...mockLisCertificate,
          lowIncomeScheme: {
            partnerCitizenId: MOCK_CITIZEN_PARTNER_ID,
          },
        },
        mockMandatoryLisHeaders,
      ),
      params: { citizenId: MOCK_CITIZEN_ID },
    });
    expect(userPreferenceApiSpyInstance).toHaveBeenCalledTimes(2);
    expect(userPreferenceApiSpyInstance).toHaveBeenNthCalledWith(1, {
      ...buildExpectedGetRequest(
        USER_PREFERENCE_FIND_BY_CITIZEN_ID_AND_TYPE,
        mockMandatoryLisHeaders,
      ),
      params: {
        certificateType: CertificateType.LIS_HC2,
        citizenId: MOCK_CITIZEN_PARTNER_ID,
      },
    });
    expect(userPreferenceApiSpyInstance).toHaveBeenNthCalledWith(2, {
      ...buildExpectedPostRequest(
        USER_PREFERENCE_BASE_URL_PATH,
        {
          certificateType: CertificateType.LIS_HC2,
          event: Event.ANY,
          preference: Preference.EMAIL,
        },
        mockMandatoryLisHeaders,
      ),
      params: {
        citizenId: MOCK_CITIZEN_ID,
      },
    });
    expect(winstonInfoLoggerSpy).toHaveBeenCalledTimes(5);
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      2,
      `Matching citizen record for id:[${MOCK_CITIZEN_PARTNER_ID}]`,
    );
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      3,
      `Citizen record saved successfully, citizenId:[${MOCK_CITIZEN_ID}]`,
    );
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      4,
      "Certificate record saved successfully, certificateId:[ac1be9c0-8578-15b0-8185-7da2d6a00010, reference: [12345678]]",
    );
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      5,
      "User Preference record saved successfully, userPreferenceId:[c0a800f6-8806-16e9-8188-062879e40000]",
    );
  });

  it("should patch user preference for partner but not citizen when partner preference value change", async () => {
    // given
    citizenApiSpyInstance
      .mockImplementationOnce(() =>
        Promise.resolve(mockCitizenLisStaffFullResponse),
      )
      .mockImplementationOnce(() => Promise.resolve(mockPartnerFullResponse));
    const patchPartnerPreferenceCall = {
      ...mockUserPreferencePartnerResponse,
      prefence: Preference.POSTAL,
    };
    certificateApiSpyInstance.mockResolvedValue(mockLisCertificateResponse);

    userPreferenceApiSpyInstance
      .mockImplementationOnce(() =>
        Promise.resolve(userPreferenceGetResponseWithLisStaffChannel),
      )
      .mockImplementationOnce(() =>
        Promise.resolve(mockUserPreferencePartnerResponse),
      )
      .mockImplementationOnce(() =>
        Promise.resolve(patchPartnerPreferenceCall),
      );

    const mockPartnerInRequestBody = {
      ...mockPartnerInRequestBodyWithId,
      userPreference: {
        preference: Preference.POSTAL,
      },
    };

    // when
    const result = await issueCertificate(
      mockMandatoryLisHeaders,
      mockCitizenInRequestBodyWithId,
      mockPartnerInRequestBody,
      mockLisCertificate,
    );

    // then
    expect(result).toEqual(mockSuccessResponseWithPartner);
    expect(citizenApiSpyInstance).toHaveBeenCalledTimes(2);
    expect(citizenApiSpyInstance).toHaveBeenNthCalledWith(1, {
      ...buildExpectedGetRequest(
        CITIZEN_BASE_URL_PATH + "/" + MOCK_CITIZEN_ID,
        mockMandatoryLisHeaders,
      ),
    });
    expect(citizenApiSpyInstance).toHaveBeenNthCalledWith(
      2,
      buildExpectedGetRequest(
        CITIZEN_BASE_URL_PATH + "/" + MOCK_CITIZEN_PARTNER_ID,
        mockMandatoryLisHeaders,
      ),
    );
    expect(certificateApiSpyInstance).toHaveBeenCalledTimes(1);
    expect(certificateApiSpyInstance).toHaveBeenNthCalledWith(1, {
      ...buildExpectedPostRequest(
        CERTIFICATE_BASE_URL_PATH,
        {
          ...mockLisCertificate,
          lowIncomeScheme: {
            partnerCitizenId: MOCK_CITIZEN_PARTNER_ID,
          },
        },
        mockMandatoryLisHeaders,
      ),
      params: { citizenId: MOCK_CITIZEN_ID },
    });
    expect(userPreferenceApiSpyInstance).toHaveBeenCalledTimes(3);
    expect(userPreferenceApiSpyInstance).toHaveBeenNthCalledWith(1, {
      ...buildExpectedGetRequest(
        USER_PREFERENCE_FIND_BY_CITIZEN_ID_AND_TYPE,
        mockMandatoryLisHeaders,
      ),
      params: {
        certificateType: CertificateType.LIS_HC2,
        citizenId: MOCK_CITIZEN_ID,
      },
    });
    expect(userPreferenceApiSpyInstance).toHaveBeenNthCalledWith(2, {
      ...buildExpectedGetRequest(
        USER_PREFERENCE_FIND_BY_CITIZEN_ID_AND_TYPE,
        mockMandatoryLisHeaders,
      ),
      params: {
        certificateType: CertificateType.LIS_HC2,
        citizenId: MOCK_CITIZEN_PARTNER_ID,
      },
    });
    expect(userPreferenceApiSpyInstance).toHaveBeenNthCalledWith(
      3,
      buildExpectedPatchRequest(
        USER_PREFERENCE_BASE_URL_PATH + "/" + MOCK_USER_PREFERENCE_ID,
        {
          ...mockPatchUserPreferenceRequest,
          preference: Preference.POSTAL,
        },
        mockMandatoryLisHeaders,
      ),
    );
    expect(winstonInfoLoggerSpy).toHaveBeenCalledTimes(5);
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      2,
      `Matching citizen record for id:[${MOCK_CITIZEN_ID}]`,
    );
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      3,
      `Matching citizen record for id:[${MOCK_CITIZEN_PARTNER_ID}]`,
    );
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      4,
      "Certificate record saved successfully, certificateId:[ac1be9c0-8578-15b0-8185-7da2d6a00010, reference: [12345678]]",
    );
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      5,
      "User Preference record updated successfully preferen-cexx-idxx-xxxx-xxxxxxxxxxxx",
    );
  });

  it("should patch user preference for citizen when preference value change", async () => {
    // given
    citizenApiSpyInstance.mockImplementationOnce(() =>
      Promise.resolve(mockCitizenLisStaffFullResponse),
    );
    certificateApiSpyInstance.mockResolvedValue(mockLisCertificateResponse);
    userPreferenceApiSpyInstance
      .mockImplementationOnce(() =>
        Promise.resolve(userPreferenceGetResponseWithLisStaffChannel),
      )
      .mockImplementationOnce(() =>
        Promise.resolve({
          ...userPreferenceGetResponseWithLisStaffChannel,
          preference: Preference.POSTAL,
        }),
      );
    const mockCitizenInRequestBody = {
      ...mockCitizenInRequestBodyWithId,
      userPreference: {
        preference: Preference.POSTAL,
      },
    };

    // when
    const result = await issueCertificate(
      mockMandatoryLisHeaders,
      mockCitizenInRequestBody,
      null,
      mockLisCertificate,
    );

    // then
    expect(result).toEqual(mockSuccessResponseLisHc2);
    expect(citizenApiSpyInstance).toHaveBeenCalledTimes(1);
    expect(citizenApiSpyInstance).toHaveBeenNthCalledWith(1, {
      ...buildExpectedGetRequest(
        CITIZEN_BASE_URL_PATH + "/" + MOCK_CITIZEN_ID,
        mockMandatoryLisHeaders,
      ),
    });
    expect(certificateApiSpyInstance).toHaveBeenCalledTimes(1);
    expect(certificateApiSpyInstance).toHaveBeenNthCalledWith(1, {
      ...buildExpectedPostRequest(
        CERTIFICATE_BASE_URL_PATH,
        {
          ...mockLisCertificate,
          lowIncomeScheme: undefined,
        },
        mockMandatoryLisHeaders,
      ),
      params: {
        citizenId: MOCK_CITIZEN_ID,
      },
    });
    expect(userPreferenceApiSpyInstance).toHaveBeenCalledTimes(2);
    expect(userPreferenceApiSpyInstance).toHaveBeenNthCalledWith(1, {
      ...buildExpectedGetRequest(
        USER_PREFERENCE_FIND_BY_CITIZEN_ID_AND_TYPE,
        mockMandatoryLisHeaders,
      ),
      params: {
        certificateType: CertificateType.LIS_HC2,
        citizenId: MOCK_CITIZEN_ID,
      },
    });
    expect(userPreferenceApiSpyInstance).toHaveBeenNthCalledWith(
      2,
      buildExpectedPatchRequest(
        USER_PREFERENCE_BASE_URL_PATH + "/c0a800f6-8806-16e9-8188-062879e40000",
        {
          ...mockPatchUserPreferenceRequest,
          preference: Preference.POSTAL,
        },
        mockMandatoryLisHeaders,
      ),
    );
    expect(winstonInfoLoggerSpy).toHaveBeenCalledTimes(4);
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      2,
      `Matching citizen record for id:[${MOCK_CITIZEN_ID}]`,
    );
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      3,
      "Certificate record saved successfully, certificateId:[ac1be9c0-8578-15b0-8185-7da2d6a00010, reference: [12345678]]",
    );
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      4,
      "User Preference record updated successfully c0a800f6-8806-16e9-8188-062879e40000",
    );
  });

  it("should patch user preference for citizen and partner when preference value change", async () => {
    // given
    citizenApiSpyInstance
      .mockImplementationOnce(() =>
        Promise.resolve(mockCitizenLisStaffFullResponse),
      )
      .mockImplementationOnce(() => Promise.resolve(mockPartnerFullResponse));
    certificateApiSpyInstance.mockResolvedValue(mockLisCertificateResponse);
    const mockPreferenceRepsonseWithPostal = {
      ...userPreferenceGetResponseWithLisStaffChannel,
      preference: Preference.POSTAL,
    };
    const mockPreferencePartnerRepsonseWithPostal = {
      ...mockUserPreferencePartnerResponse,
      preference: Preference.POSTAL,
    };
    userPreferenceApiSpyInstance
      .mockImplementationOnce(() =>
        Promise.resolve(userPreferenceGetResponseWithLisStaffChannel),
      )
      .mockImplementationOnce(() =>
        Promise.resolve(mockUserPreferencePartnerResponse),
      )
      .mockImplementationOnce(() =>
        Promise.resolve(mockPreferenceRepsonseWithPostal),
      )
      .mockImplementationOnce(() =>
        Promise.resolve(mockPreferencePartnerRepsonseWithPostal),
      );
    const mockCitizenInRequestBody = {
      ...mockCitizenInRequestBodyWithId,
      userPreference: {
        preference: Preference.POSTAL,
      },
    };

    const mockPartnerInRequestBody = {
      ...mockPartnerInRequestBodyWithId,
      userPreference: {
        preference: Preference.POSTAL,
      },
    };

    // when
    const result = await issueCertificate(
      mockMandatoryLisHeaders,
      mockCitizenInRequestBody,
      mockPartnerInRequestBody,
      mockLisCertificate,
    );

    // then
    expect(result).toEqual(mockSuccessResponseWithPartnerPatchedPreference);
    expect(citizenApiSpyInstance).toHaveBeenCalledTimes(2);
    expect(citizenApiSpyInstance).toHaveBeenNthCalledWith(1, {
      ...buildExpectedGetRequest(
        CITIZEN_BASE_URL_PATH + "/" + MOCK_CITIZEN_ID,
        mockMandatoryLisHeaders,
      ),
    });
    expect(citizenApiSpyInstance).toHaveBeenNthCalledWith(2, {
      ...buildExpectedGetRequest(
        CITIZEN_BASE_URL_PATH + "/" + MOCK_CITIZEN_PARTNER_ID,
        mockMandatoryLisHeaders,
      ),
    });
    expect(certificateApiSpyInstance).toHaveBeenCalledTimes(1);
    expect(certificateApiSpyInstance).toHaveBeenNthCalledWith(1, {
      ...buildExpectedPostRequest(
        CERTIFICATE_BASE_URL_PATH,
        {
          ...mockLisCertificate,
          lowIncomeScheme: {
            partnerCitizenId: MOCK_CITIZEN_PARTNER_ID,
          },
        },
        mockMandatoryLisHeaders,
      ),
      params: {
        citizenId: MOCK_CITIZEN_ID,
      },
    });
    expect(userPreferenceApiSpyInstance).toHaveBeenCalledTimes(4);
    expect(userPreferenceApiSpyInstance).toHaveBeenNthCalledWith(1, {
      ...buildExpectedGetRequest(
        USER_PREFERENCE_FIND_BY_CITIZEN_ID_AND_TYPE,
        mockMandatoryLisHeaders,
      ),
      params: {
        certificateType: CertificateType.LIS_HC2,
        citizenId: MOCK_CITIZEN_ID,
      },
    });
    expect(userPreferenceApiSpyInstance).toHaveBeenNthCalledWith(2, {
      ...buildExpectedGetRequest(
        USER_PREFERENCE_FIND_BY_CITIZEN_ID_AND_TYPE,
        mockMandatoryLisHeaders,
      ),
      params: {
        certificateType: CertificateType.LIS_HC2,
        citizenId: MOCK_CITIZEN_PARTNER_ID,
      },
    });
    expect(userPreferenceApiSpyInstance).toHaveBeenNthCalledWith(
      3,
      buildExpectedPatchRequest(
        USER_PREFERENCE_BASE_URL_PATH + "/c0a800f6-8806-16e9-8188-062879e40000",
        {
          ...mockPatchUserPreferenceRequest,
          preference: Preference.POSTAL,
        },
        mockMandatoryLisHeaders,
      ),
    );
    expect(userPreferenceApiSpyInstance).toHaveBeenNthCalledWith(
      4,
      buildExpectedPatchRequest(
        USER_PREFERENCE_BASE_URL_PATH + "/" + MOCK_USER_PREFERENCE_ID,
        {
          ...mockPatchUserPreferenceRequest,
          preference: Preference.POSTAL,
        },
        mockMandatoryLisHeaders,
      ),
    );
    expect(winstonInfoLoggerSpy).toHaveBeenCalledTimes(6);
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      2,
      `Matching citizen record for id:[${MOCK_CITIZEN_ID}]`,
    );
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      3,
      `Matching citizen record for id:[${MOCK_CITIZEN_PARTNER_ID}]`,
    );
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      4,
      "Certificate record saved successfully, certificateId:[ac1be9c0-8578-15b0-8185-7da2d6a00010, reference: [12345678]]",
    );
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      5,
      "User Preference record updated successfully c0a800f6-8806-16e9-8188-062879e40000",
    );
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      6,
      "User Preference record updated successfully preferen-cexx-idxx-xxxx-xxxxxxxxxxxx",
    );
  });
});
