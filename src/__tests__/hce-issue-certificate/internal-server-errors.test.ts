import { InternalError } from "@nhsbsa/health-charge-exemption-npm-utils-rest";
import {
  mockLisCertificateResponse,
  mockLisCertificate,
  mockCertificate,
  mockCertificateResponse,
} from "../../__mocks__/certificate/certificate-mocks";
import {
  mockCitizenLisStaffFullResponse,
  mockCitizenResponse,
  mockPartnerCitizenResponse,
  mockPartnerFullResponse,
} from "../../__mocks__/citizen/citizen-mocks";
import {
  mockUserPreferencePartnerResponse,
  mockUserPreferenceResponse,
} from "../../__mocks__/user-preference/user-preference-mocks";
import { issueCertificate } from "../../hce-issue-certificate";
import {
  certificateApiSpyInstance,
  citizenApiSpyInstance,
  userPreferenceApiSpyInstance,
  winstonInfoLoggerSpy,
} from "../../../test-setup/setup";
import {
  mockedDate,
  mockMandatoryLisHeaders,
  citizenBaseSchemaMock,
  partnerBaseSchemaMock,
  CITIZEN_BASE_URL_PATH,
  MOCK_CITIZEN_ID,
  SERVICE_ENTERED_LOG_MESSAGE,
  CERTIFICATE_BASE_URL_PATH,
  MOCK_CITIZEN_PARTNER_ID,
  USER_PREFERENCE_BASE_URL_PATH,
  mockMandatoryHeaders,
  mockedError,
  mockPostUserPreferenceRequest,
  MOCK_USER_PREFERENCE_ID,
  mockedDuplicateError,
} from "../utils/test-constants";
import { buildExpectedPostRequest } from "../utils/test-functions";
import {
  CertificateType,
  Channel,
} from "@nhsbsa/health-charge-exemption-npm-common-models";

beforeEach(() => {
  // mock setup / clear
  jest.clearAllMocks();
  jest.resetAllMocks();
  jest.useFakeTimers().setSystemTime(mockedDate);
});

describe("issueCertificate", () => {
  describe("citizenApi", () => {
    it("should throw an internal server error when citizen api has an error response", async () => {
      // given
      citizenApiSpyInstance.mockRejectedValueOnce(mockedError);

      // when / then
      await expect(async () =>
        issueCertificate(
          mockMandatoryLisHeaders,
          citizenBaseSchemaMock,
          partnerBaseSchemaMock,
          mockLisCertificate,
        ),
      ).rejects.toThrow(new InternalError(mockedError.message, mockedDate));
      expect(citizenApiSpyInstance).toHaveBeenCalledTimes(1);
      expect(citizenApiSpyInstance).toHaveBeenNthCalledWith(1, {
        ...buildExpectedPostRequest(
          CITIZEN_BASE_URL_PATH,
          citizenBaseSchemaMock,
          mockMandatoryLisHeaders,
        ),
      });
      expect(certificateApiSpyInstance).not.toHaveBeenCalled();
      expect(userPreferenceApiSpyInstance).not.toHaveBeenCalled();
      expect(winstonInfoLoggerSpy).toHaveBeenCalledTimes(1);
    });

    it("should throw an internal server error when citizen api has an error response for partner", async () => {
      // given
      citizenApiSpyInstance
        .mockImplementationOnce(() =>
          Promise.resolve(mockCitizenLisStaffFullResponse),
        )
        .mockRejectedValueOnce(mockedError);

      // when / then
      await expect(async () =>
        issueCertificate(
          mockMandatoryLisHeaders,
          citizenBaseSchemaMock,
          partnerBaseSchemaMock,
          mockLisCertificate,
        ),
      ).rejects.toThrow(new InternalError(mockedError.message, mockedDate));
      expect(citizenApiSpyInstance).toHaveBeenCalledTimes(2);
      expect(citizenApiSpyInstance).toHaveBeenNthCalledWith(1, {
        ...buildExpectedPostRequest(
          CITIZEN_BASE_URL_PATH,
          citizenBaseSchemaMock,
          mockMandatoryLisHeaders,
        ),
      });
      expect(citizenApiSpyInstance).toHaveBeenNthCalledWith(2, {
        ...buildExpectedPostRequest(
          CITIZEN_BASE_URL_PATH,
          partnerBaseSchemaMock,
          mockMandatoryLisHeaders,
        ),
      });
      expect(certificateApiSpyInstance).not.toHaveBeenCalled();
      expect(userPreferenceApiSpyInstance).not.toHaveBeenCalled();
      expect(winstonInfoLoggerSpy).toHaveBeenCalledTimes(2);
      expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
        2,
        `Citizen record saved successfully, citizenId:[${MOCK_CITIZEN_ID}]`,
      );
    });
  });

  describe("certificateApi", () => {
    afterEach(() => {
      expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
        1,
        SERVICE_ENTERED_LOG_MESSAGE,
      );
    });

    it("should throw an internal server error when certificate api has an error response after citizen creation", async () => {
      // given
      citizenApiSpyInstance
        .mockImplementationOnce(() => Promise.resolve(mockCitizenResponse))
        .mockImplementationOnce(() =>
          Promise.resolve(mockPartnerCitizenResponse),
        );
      userPreferenceApiSpyInstance.mockResolvedValue(
        mockUserPreferenceResponse,
      );
      userPreferenceApiSpyInstance.mockResolvedValue(
        mockUserPreferencePartnerResponse,
      );
      certificateApiSpyInstance.mockRejectedValueOnce(mockedError);

      // when / then
      await expect(async () =>
        issueCertificate(
          mockMandatoryLisHeaders,
          citizenBaseSchemaMock,
          partnerBaseSchemaMock,
          mockLisCertificate,
        ),
      ).rejects.toThrow(new InternalError(mockedError.message, mockedDate));
      expect(citizenApiSpyInstance).toHaveBeenCalledTimes(2);
      expect(citizenApiSpyInstance).toHaveBeenNthCalledWith(1, {
        ...buildExpectedPostRequest(
          CITIZEN_BASE_URL_PATH,
          citizenBaseSchemaMock,
          mockMandatoryLisHeaders,
        ),
      });
      expect(citizenApiSpyInstance).toHaveBeenNthCalledWith(2, {
        ...buildExpectedPostRequest(
          CITIZEN_BASE_URL_PATH,
          partnerBaseSchemaMock,
          mockMandatoryLisHeaders,
        ),
      });
      expect(certificateApiSpyInstance).toHaveBeenCalledTimes(1);
      expect(certificateApiSpyInstance).toHaveBeenNthCalledWith(1, {
        ...buildExpectedPostRequest(
          CERTIFICATE_BASE_URL_PATH,
          {
            ...mockLisCertificate,
            lowIncomeScheme: {
              partnerCitizenId: MOCK_CITIZEN_PARTNER_ID,
            },
          },
          mockMandatoryLisHeaders,
        ),
        params: {
          citizenId: MOCK_CITIZEN_ID,
        },
      });
      expect(userPreferenceApiSpyInstance).toHaveBeenCalledTimes(2);
      expect(userPreferenceApiSpyInstance).toHaveBeenNthCalledWith(1, {
        ...buildExpectedPostRequest(
          USER_PREFERENCE_BASE_URL_PATH,
          mockPostUserPreferenceRequest,
          mockMandatoryLisHeaders,
        ),
        params: {
          citizenId: MOCK_CITIZEN_ID,
        },
      });
      expect(userPreferenceApiSpyInstance).toHaveBeenNthCalledWith(2, {
        ...buildExpectedPostRequest(
          USER_PREFERENCE_BASE_URL_PATH,
          mockPostUserPreferenceRequest,
          mockMandatoryLisHeaders,
        ),
        params: {
          citizenId: MOCK_CITIZEN_PARTNER_ID,
        },
      });
      expect(winstonInfoLoggerSpy).toHaveBeenCalledTimes(5);
      expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
        2,
        `Citizen record saved successfully, citizenId:[${MOCK_CITIZEN_ID}]`,
      );
      expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
        3,
        `Citizen record saved for partner successfully, citizenId:[${MOCK_CITIZEN_PARTNER_ID}]`,
      );
      expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
        4,
        `User Preference record saved successfully, userPreferenceId:[${MOCK_USER_PREFERENCE_ID}]`,
      );
      expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
        5,
        `User Preference record saved successfully, userPreferenceId:[${MOCK_USER_PREFERENCE_ID}]`,
      );
    });

    it("should throw conflict when duplicate reference found", async () => {
      // given
      citizenApiSpyInstance.mockImplementationOnce(() =>
        Promise.resolve(mockCitizenResponse),
      );
      userPreferenceApiSpyInstance.mockResolvedValue(
        mockUserPreferenceResponse,
      );
      certificateApiSpyInstance.mockRejectedValueOnce(mockedDuplicateError);
      const mockCertificateWithReference = {
        ...mockCertificate,
        reference: "12345678",
      };

      // when
      await expect(async () =>
        issueCertificate(
          mockMandatoryHeaders,
          citizenBaseSchemaMock,
          null,
          mockCertificateWithReference,
        ),
      ).rejects.toEqual(mockedDuplicateError);

      // then
      expect(citizenApiSpyInstance).toHaveBeenCalledTimes(1);
      expect(citizenApiSpyInstance).toHaveBeenNthCalledWith(1, {
        ...buildExpectedPostRequest(
          CITIZEN_BASE_URL_PATH,
          citizenBaseSchemaMock,
          mockMandatoryHeaders,
        ),
      });
      expect(userPreferenceApiSpyInstance).toHaveBeenCalledTimes(1);
      expect(userPreferenceApiSpyInstance).toHaveBeenNthCalledWith(1, {
        ...buildExpectedPostRequest(
          USER_PREFERENCE_BASE_URL_PATH,
          {
            ...mockPostUserPreferenceRequest,
            certificateType: CertificateType.HRT_PPC,
          },
          mockMandatoryHeaders,
        ),
        params: {
          citizenId: MOCK_CITIZEN_ID,
        },
      });
      expect(certificateApiSpyInstance).toHaveBeenCalledTimes(1);
      expect(certificateApiSpyInstance).toHaveBeenNthCalledWith(1, {
        ...buildExpectedPostRequest(
          CERTIFICATE_BASE_URL_PATH,
          mockCertificateWithReference,
          mockMandatoryHeaders,
        ),
        params: {
          citizenId: MOCK_CITIZEN_ID,
        },
      });
      expect(winstonInfoLoggerSpy).toHaveBeenCalledTimes(3);
      expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
        2,
        `Citizen record saved successfully, citizenId:[${MOCK_CITIZEN_ID}]`,
      );
      expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
        3,
        `User Preference record saved successfully, userPreferenceId:[c0a800f6-8806-16e9-8188-062879e40000]`,
      );
    });
  });

  describe("userPreferenceApi", () => {
    afterEach(() => {
      expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
        1,
        SERVICE_ENTERED_LOG_MESSAGE,
      );
    });

    it("should throw an internal server error when user preference api has an error response", async () => {
      // given
      citizenApiSpyInstance.mockResolvedValue(mockCitizenResponse);
      certificateApiSpyInstance.mockResolvedValue(mockCertificateResponse);
      userPreferenceApiSpyInstance.mockResolvedValue(
        mockUserPreferenceResponse,
      );
      userPreferenceApiSpyInstance.mockRejectedValueOnce(mockedError);

      // when / then
      await expect(async () =>
        issueCertificate(
          mockMandatoryHeaders,
          citizenBaseSchemaMock,
          null,
          mockCertificate,
        ),
      ).rejects.toThrow(new InternalError(mockedError.message, mockedDate));
      expect(certificateApiSpyInstance).toHaveBeenCalledTimes(1);
      expect(certificateApiSpyInstance).toHaveBeenNthCalledWith(1, {
        ...buildExpectedPostRequest(
          CERTIFICATE_BASE_URL_PATH,
          mockCertificate,
          mockMandatoryHeaders,
        ),
        params: { citizenId: MOCK_CITIZEN_ID },
      });
      expect(userPreferenceApiSpyInstance).toHaveBeenCalledTimes(1);
      expect(userPreferenceApiSpyInstance).toHaveBeenNthCalledWith(1, {
        ...buildExpectedPostRequest(
          USER_PREFERENCE_BASE_URL_PATH,
          {
            ...mockPostUserPreferenceRequest,
            certificateType: CertificateType.HRT_PPC,
          },
          mockMandatoryHeaders,
        ),
        params: { citizenId: MOCK_CITIZEN_ID },
      });
      expect(winstonInfoLoggerSpy).toHaveBeenCalledTimes(3);
      expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
        2,
        `Citizen record saved successfully, citizenId:[${MOCK_CITIZEN_ID}]`,
      );
      expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
        3,
        "Certificate record saved successfully, certificateId:[ac1be9c0-8578-15b0-8185-7da2d6a00010, reference: [3569BDF217A46923DB1A]]",
      );
    });

    // process flow: citizen requests -> certificate request -> user-preference for citizen -> user-preference for partner = fails
    it("should throw an internal server error when user-preference api POST has an error response in process", async () => {
      // given
      citizenApiSpyInstance
        .mockImplementationOnce(() =>
          Promise.resolve(mockCitizenLisStaffFullResponse),
        )
        .mockImplementationOnce(() => Promise.resolve(mockPartnerFullResponse));
      certificateApiSpyInstance.mockResolvedValue(mockLisCertificateResponse);
      userPreferenceApiSpyInstance.mockImplementationOnce(() =>
        Promise.resolve({
          ...mockUserPreferenceResponse,
          certificateType: CertificateType.LIS_HC2,
          _meta: {
            ...mockUserPreferenceResponse._meta,
            channel: Channel.LIS_STAFF,
          },
        }),
      );
      userPreferenceApiSpyInstance.mockRejectedValueOnce(mockedError);

      // when / then
      await expect(async () =>
        issueCertificate(
          mockMandatoryLisHeaders,
          citizenBaseSchemaMock,
          partnerBaseSchemaMock,
          mockLisCertificate,
        ),
      ).rejects.toThrow(new InternalError(mockedError.message, mockedDate));
      expect(citizenApiSpyInstance).toHaveBeenCalledTimes(2);
      expect(citizenApiSpyInstance).toHaveBeenNthCalledWith(1, {
        ...buildExpectedPostRequest(
          CITIZEN_BASE_URL_PATH,
          citizenBaseSchemaMock,
          mockMandatoryLisHeaders,
        ),
      });
      expect(citizenApiSpyInstance).toHaveBeenNthCalledWith(2, {
        ...buildExpectedPostRequest(
          CITIZEN_BASE_URL_PATH,
          partnerBaseSchemaMock,
          mockMandatoryLisHeaders,
        ),
      });
      expect(certificateApiSpyInstance).toHaveBeenCalledTimes(1);
      expect(certificateApiSpyInstance).toHaveBeenNthCalledWith(1, {
        ...buildExpectedPostRequest(
          CERTIFICATE_BASE_URL_PATH,
          {
            ...mockLisCertificate,
            lowIncomeScheme: {
              partnerCitizenId: MOCK_CITIZEN_PARTNER_ID,
            },
          },
          mockMandatoryLisHeaders,
        ),
        params: {
          citizenId: MOCK_CITIZEN_ID,
        },
      });
      expect(userPreferenceApiSpyInstance).toHaveBeenCalledTimes(2);
      expect(userPreferenceApiSpyInstance).toHaveBeenNthCalledWith(1, {
        ...buildExpectedPostRequest(
          USER_PREFERENCE_BASE_URL_PATH,
          mockPostUserPreferenceRequest,
          mockMandatoryLisHeaders,
        ),
        params: {
          citizenId: MOCK_CITIZEN_ID,
        },
      });
      expect(userPreferenceApiSpyInstance).toHaveBeenNthCalledWith(2, {
        ...buildExpectedPostRequest(
          USER_PREFERENCE_BASE_URL_PATH,
          mockPostUserPreferenceRequest,
          mockMandatoryLisHeaders,
        ),
        params: {
          citizenId: MOCK_CITIZEN_PARTNER_ID,
        },
      });
      expect(winstonInfoLoggerSpy).toHaveBeenCalledTimes(5);
      expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
        2,
        `Citizen record saved successfully, citizenId:[${MOCK_CITIZEN_ID}]`,
      );
      expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
        3,
        `Citizen record saved for partner successfully, citizenId:[${MOCK_CITIZEN_PARTNER_ID}]`,
      );
      expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
        4,
        "Certificate record saved successfully, certificateId:[ac1be9c0-8578-15b0-8185-7da2d6a00010, reference: [12345678]]",
      );
      expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
        5,
        "User Preference record saved successfully, userPreferenceId:[c0a800f6-8806-16e9-8188-062879e40000]",
      );
    });
  });
});
