import { BadRequest } from "@nhsbsa/health-charge-exemption-npm-utils-rest";
import {
  citizenApiSpyInstance,
  userPreferenceApiSpyInstance,
  certificateApiSpyInstance,
  winstonInfoLoggerSpy,
} from "../../../test-setup/setup";
import { mockLisCertificate } from "../../__mocks__/certificate/certificate-mocks";
import {
  mockCitizenLisStaffFullResponse,
  mockPartnerFullResponse,
} from "../../__mocks__/citizen/citizen-mocks";
import {
  mockUserPreferencePartnerResponse,
  mockUserPreferenceResponse,
} from "../../__mocks__/user-preference/user-preference-mocks";
import { issueCertificate } from "../../hce-issue-certificate";
import {
  mockedDate,
  SERVICE_ENTERED_LOG_MESSAGE,
  CITIZEN_BASE_URL_PATH,
  MOCK_CITIZEN_ID,
  mockMandatoryLisHeaders,
  USER_PREFERENCE_FIND_BY_CITIZEN_ID_AND_TYPE,
  mockCitizenInRequestBodyWithId,
  RECORD_MISMATCH_VALIDATION_MESSAGE,
  NOT_IN_DATABASE_VALIDATION_MESSAGE,
  partnerBaseSchemaMock,
  citizenBaseSchemaMock,
  DOES_NOT_MATCH_VALIDATION_MESSAGE,
  mockPartnerInRequestBodyWithId,
  MOCK_CITIZEN_PARTNER_ID,
  PARTNER_FIELD_KEY,
  CITIZEN_FIELD_KEY,
} from "../utils/test-constants";
import { buildExpectedGetRequest } from "../utils/test-functions";
import { CertificateType } from "@nhsbsa/health-charge-exemption-npm-common-models";

beforeEach(() => {
  // mock setup / clear
  jest.clearAllMocks();
  jest.resetAllMocks();
  jest.useFakeTimers().setSystemTime(mockedDate);
});

describe("issueCertificate", () => {
  afterEach(() => {
    // then
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      1,
      SERVICE_ENTERED_LOG_MESSAGE,
    );
  });

  describe("bad requests with same api calls", () => {
    afterEach(() => {
      // then expect
      expect(citizenApiSpyInstance).toHaveBeenCalledTimes(1);
      expect(citizenApiSpyInstance).toHaveBeenNthCalledWith(1, {
        ...buildExpectedGetRequest(
          CITIZEN_BASE_URL_PATH + "/" + MOCK_CITIZEN_ID,
          mockMandatoryLisHeaders,
        ),
      });
      expect(certificateApiSpyInstance).not.toHaveBeenCalled();
      expect(userPreferenceApiSpyInstance).toHaveBeenCalledTimes(1);
      expect(userPreferenceApiSpyInstance).toHaveBeenNthCalledWith(1, {
        ...buildExpectedGetRequest(
          USER_PREFERENCE_FIND_BY_CITIZEN_ID_AND_TYPE,
          mockMandatoryLisHeaders,
        ),
        params: {
          certificateType: CertificateType.LIS_HC2,
          citizenId: MOCK_CITIZEN_ID,
        },
      });
    });

    it("should throw bad request when citizen id not found in database", async () => {
      // given
      citizenApiSpyInstance.mockImplementationOnce(() => Promise.resolve({}));
      userPreferenceApiSpyInstance.mockImplementationOnce(() =>
        Promise.resolve({}),
      );

      // when
      await expect(async () =>
        issueCertificate(
          mockMandatoryLisHeaders,
          mockCitizenInRequestBodyWithId,
          null,
          mockLisCertificate,
        ),
      ).rejects.toThrow(
        expect.objectContaining({
          message: RECORD_MISMATCH_VALIDATION_MESSAGE,
          timestamp: mockedDate,
          fieldErrors: [
            {
              field: CITIZEN_FIELD_KEY + ".id",
              message: NOT_IN_DATABASE_VALIDATION_MESSAGE,
            },
          ],
        }),
      );

      // then
      expect(winstonInfoLoggerSpy).toHaveBeenCalledTimes(1);
    });

    it("should throw bad request when partner id not found in database", async () => {
      // given
      citizenApiSpyInstance.mockImplementationOnce(() => Promise.resolve({}));
      userPreferenceApiSpyInstance.mockImplementationOnce(() =>
        Promise.resolve({}),
      );
      const mockPartnerInRequestBody = {
        ...partnerBaseSchemaMock,
        id: MOCK_CITIZEN_ID,
      };

      // when
      await expect(async () =>
        issueCertificate(
          mockMandatoryLisHeaders,
          citizenBaseSchemaMock,
          mockPartnerInRequestBody,
          mockLisCertificate,
        ),
      ).rejects.toThrow(
        expect.objectContaining({
          message: RECORD_MISMATCH_VALIDATION_MESSAGE,
          timestamp: mockedDate,
          fieldErrors: [
            {
              field: PARTNER_FIELD_KEY + ".id",
              message: NOT_IN_DATABASE_VALIDATION_MESSAGE,
            },
          ],
        }),
      );

      // then
      expect(winstonInfoLoggerSpy).toHaveBeenCalledTimes(1);
    });

    it("should throw bad request when citizen does not match existing citizen", async () => {
      // given
      citizenApiSpyInstance.mockImplementationOnce(() =>
        Promise.resolve(mockCitizenLisStaffFullResponse),
      );
      userPreferenceApiSpyInstance.mockImplementationOnce(() =>
        Promise.resolve({
          ...mockUserPreferenceResponse,
          certificateType: CertificateType.LIS_HC2,
        }),
      );
      const mockCitizenInRequestBody = {
        ...mockCitizenInRequestBodyWithId,
        firstName: "arcwo",
      };

      // when
      await expect(async () =>
        issueCertificate(
          mockMandatoryLisHeaders,
          mockCitizenInRequestBody,
          null,
          mockLisCertificate,
        ),
      ).rejects.toEqual(
        new BadRequest(RECORD_MISMATCH_VALIDATION_MESSAGE, mockedDate, [
          {
            field: CITIZEN_FIELD_KEY + ".firstName",
            message: DOES_NOT_MATCH_VALIDATION_MESSAGE,
          },
        ]),
      );

      // then
      expect(winstonInfoLoggerSpy).toHaveBeenCalledTimes(2);
      expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
        2,
        `Matching citizen record for id:[${MOCK_CITIZEN_ID}]`,
      );
    });
  });

  it("should throw bad request when partner does not match existing partner", async () => {
    // given
    citizenApiSpyInstance.mockImplementationOnce(() =>
      Promise.resolve(mockPartnerFullResponse),
    );
    userPreferenceApiSpyInstance.mockImplementationOnce(() =>
      Promise.resolve(mockUserPreferencePartnerResponse),
    );
    const mockPartnerInRequestBody = {
      ...mockPartnerInRequestBodyWithId,
      firstName: "arcwo",
    };

    // when
    await expect(async () =>
      issueCertificate(
        mockMandatoryLisHeaders,
        citizenBaseSchemaMock,
        mockPartnerInRequestBody,
        mockLisCertificate,
      ),
    ).rejects.toEqual(
      new BadRequest(RECORD_MISMATCH_VALIDATION_MESSAGE, mockedDate, [
        {
          field: CITIZEN_FIELD_KEY + ".firstName",
          message: DOES_NOT_MATCH_VALIDATION_MESSAGE,
        },
      ]),
    );

    // then
    expect(citizenApiSpyInstance).toHaveBeenCalledTimes(1);
    expect(citizenApiSpyInstance).toHaveBeenNthCalledWith(1, {
      ...buildExpectedGetRequest(
        CITIZEN_BASE_URL_PATH + "/" + MOCK_CITIZEN_PARTNER_ID,
        mockMandatoryLisHeaders,
      ),
    });
    expect(certificateApiSpyInstance).not.toHaveBeenCalled();
    expect(userPreferenceApiSpyInstance).toHaveBeenCalledTimes(1);
    expect(userPreferenceApiSpyInstance).toHaveBeenNthCalledWith(1, {
      ...buildExpectedGetRequest(
        USER_PREFERENCE_FIND_BY_CITIZEN_ID_AND_TYPE,
        mockMandatoryLisHeaders,
      ),
      params: {
        certificateType: CertificateType.LIS_HC2,
        citizenId: MOCK_CITIZEN_PARTNER_ID,
      },
    });
    expect(winstonInfoLoggerSpy).toHaveBeenCalledTimes(2);
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      2,
      `Matching citizen record for id:[${MOCK_CITIZEN_PARTNER_ID}]`,
    );
  });

  it("should throw bad request when citizen and partner dont match", async () => {
    // given
    citizenApiSpyInstance.mockImplementationOnce(() =>
      Promise.resolve(mockCitizenLisStaffFullResponse),
    );
    citizenApiSpyInstance.mockImplementationOnce(() =>
      Promise.resolve(mockPartnerFullResponse),
    );
    userPreferenceApiSpyInstance
      .mockImplementationOnce(() =>
        Promise.resolve({
          ...mockUserPreferenceResponse,
          certificateType: CertificateType.LIS_HC2,
        }),
      )
      .mockImplementationOnce(() =>
        Promise.resolve({ mockUserPreferencePartnerResponse }),
      );
    const mockCitizenInRequestBody = {
      ...mockCitizenInRequestBodyWithId,
      firstName: "arcwo",
    };
    const mockPartnerInRequestBody = {
      ...mockPartnerInRequestBodyWithId,
      firstName: "arcwo",
    };

    // when
    await expect(async () =>
      issueCertificate(
        mockMandatoryLisHeaders,
        mockCitizenInRequestBody,
        mockPartnerInRequestBody,
        mockLisCertificate,
      ),
    ).rejects.toEqual(
      new BadRequest(RECORD_MISMATCH_VALIDATION_MESSAGE, mockedDate, [
        {
          field: CITIZEN_FIELD_KEY + ".firstName",
          message: DOES_NOT_MATCH_VALIDATION_MESSAGE,
        },
        {
          field: PARTNER_FIELD_KEY + ".firstName",
          message: DOES_NOT_MATCH_VALIDATION_MESSAGE,
        },
      ]),
    );

    // then
    expect(winstonInfoLoggerSpy).toHaveBeenCalledTimes(2);
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      2,
      `Matching citizen record for id:[${MOCK_CITIZEN_ID}]`,
    );
  });
});
