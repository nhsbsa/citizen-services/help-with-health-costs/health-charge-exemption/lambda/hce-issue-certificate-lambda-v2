import {
  DuplicateError,
  MandatoryHeaders,
} from "@nhsbsa/health-charge-exemption-npm-utils-rest";
import {
  mockCitizen,
  mockPartnerCitizen,
} from "../../__mocks__/citizen/citizen-mocks";
import { mockHeaders } from "../../__mocks__/mock-data";
import { citizenBaseSchema } from "../../schemas";
import {
  CertificateType,
  UserPreferencePostRequest,
  Channel,
  Event,
  Preference,
  UserPreferencePatchRequest,
} from "@nhsbsa/health-charge-exemption-npm-common-models";

export const SERVICE_ENTERED_LOG_MESSAGE = "Entered issue certificate service";
export const RECORD_MISMATCH_VALIDATION_MESSAGE =
  "The provided details do not match with the existing citizen record";
export const DOES_NOT_MATCH_VALIDATION_MESSAGE = "does not match";
export const NOT_IN_DATABASE_VALIDATION_MESSAGE = "is not stored in database";
export const USER_PREFERENCE_NO_EMAIL_VALIDATION_MESSAGE =
  "user preference set to EMAIL but no email address provided";
export const MUST_NOT_BE_NULL_OR_EMPTY_VALIDATION_MESSAGE =
  "must not be null or empty";
export const MOCK_CITIZEN_ID = "0a004b01-857c-1e59-8185-7da2d678004e";
export const MOCK_CITIZEN_PARTNER_ID = "partnerx-idxx-xxxx-xxxx-xxxxxxxxxxxx";
export const MOCK_USER_PREFERENCE_ID = "preferen-cexx-idxx-xxxx-xxxxxxxxxxxx";
export const PRODUCT_ID = "productxx-idxx-idxx-xxxx-xxxxxxxxxxxx";

export const CITIZEN_FIELD_KEY = "citizen";
export const PARTNER_FIELD_KEY = "partner";

export const CITIZEN_BASE_URL_PATH = "/v1/citizens";
export const CERTIFICATE_BASE_URL_PATH = "/v1/certificates";
export const USER_PREFERENCE_BASE_URL_PATH = "/v1/user-preference";
export const USER_PREFERENCE_FIND_BY_CITIZEN_ID_AND_TYPE =
  USER_PREFERENCE_BASE_URL_PATH + "/search/findByCitizenIdAndCertificateType";

export const citizenBaseSchemaMock = citizenBaseSchema.parse(mockCitizen);
export const partnerBaseSchemaMock =
  citizenBaseSchema.parse(mockPartnerCitizen);
export const mockCitizenInRequestBodyWithId = {
  ...citizenBaseSchemaMock,
  id: MOCK_CITIZEN_ID,
};

export const mockPartnerInRequestBodyWithId = {
  ...partnerBaseSchemaMock,
  id: MOCK_CITIZEN_PARTNER_ID,
};
export const mockMandatoryHeaders = mockHeaders as MandatoryHeaders;
export const mockMandatoryLisHeaders = {
  ...mockHeaders,
  channel: Channel.LIS_STAFF,
};
export const mockPostUserPreferenceRequest: UserPreferencePostRequest = {
  certificateType: CertificateType.LIS_HC2,
  event: Event.ANY,
  preference: Preference.EMAIL,
};

export const mockPatchUserPreferenceRequest: UserPreferencePatchRequest = {
  event: Event.ANY,
  preference: Preference.EMAIL,
};
export const mockedDate = new Date("2020-01-01");
export const mockedError = new Error("mockError");
export const mockedDuplicateError = new DuplicateError(
  "Duplicate record error",
  mockedDate,
);
