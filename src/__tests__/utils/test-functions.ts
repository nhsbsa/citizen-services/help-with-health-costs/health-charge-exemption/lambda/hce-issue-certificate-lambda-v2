import { UserPreferencePatchRequest } from "@nhsbsa/health-charge-exemption-npm-common-models";
import { MandatoryHeaders } from "@nhsbsa/health-charge-exemption-npm-utils-rest";

const defaultRequestParams = {
  responseType: "json",
};

// helper functions
export const buildExpectedPostRequest = (
  urlPath: string,
  requestData: object,
  headers: MandatoryHeaders,
) => {
  return {
    headers,
    ...defaultRequestParams,
    data: requestData,
    method: "POST",
    url: urlPath,
  };
};

export const buildExpectedGetRequest = (
  urlPath: string,
  headers: MandatoryHeaders,
) => {
  return {
    headers,
    ...defaultRequestParams,
    method: "GET",
    url: urlPath,
  };
};

export const buildExpectedPatchRequest = (
  urlPath: string,
  requestData: UserPreferencePatchRequest | object,
  headers: MandatoryHeaders,
) => {
  return {
    headers,
    ...defaultRequestParams,
    data: requestData,
    method: "PATCH",
    url: urlPath,
  };
};
