import { handler } from "../index";
import { issueCertificate } from "../hce-issue-certificate";
import {
  mockHeaders,
  mockSuccessResponse,
  mockSuccessResponseWithPartner,
} from "../__mocks__/mock-data";
import mockEvent from "../../events/event.json";
import mockEventWithPartner from "../../events/event-with-partner.json";
import { APIGatewayProxyEvent } from "aws-lambda";
import { citizenBaseSchema } from "../schemas";
import { generateMock } from "@anatine/zod-mock";
import { winstonInfoLoggerSpy } from "../../test-setup/setup";
import {
  mockCitizen,
  mockPartnerCitizen,
} from "../__mocks__/citizen/citizen-mocks";
import {
  mockCertificate,
  mockLisCertificate,
} from "../__mocks__/certificate/certificate-mocks";
import {
  mockedDate,
  USER_PREFERENCE_NO_EMAIL_VALIDATION_MESSAGE,
  mockMandatoryLisHeaders,
  PRODUCT_ID,
  CITIZEN_FIELD_KEY,
  PARTNER_FIELD_KEY,
  MUST_NOT_BE_NULL_OR_EMPTY_VALIDATION_MESSAGE,
} from "./utils/test-constants";
import {
  CertificateType,
  Preference,
} from "@nhsbsa/health-charge-exemption-npm-common-models";

jest.mock("../hce-issue-certificate");

let event: APIGatewayProxyEvent;
const mockIssueCertificate = issueCertificate as jest.MockedFunction<
  typeof issueCertificate
>;
const badRequestBaseResponseBody = {
  status: 400,
  message: "There were validation issues with the request",
  timestamp: "2020-01-01T00:00:00.000Z",
};

beforeEach(() => {
  jest.useFakeTimers().setSystemTime(mockedDate);
  mockIssueCertificate.mockClear();
});

afterEach(() => {
  // then / expect these logs for all scenarios
  expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
    1,
    "hce-issue-certificate-lambda-v2",
  );
});

describe("handler", () => {
  beforeEach(() => {
    event = JSON.parse(JSON.stringify(mockEvent));
    mockIssueCertificate.mockClear();
    mockIssueCertificate.mockResolvedValue(mockSuccessResponse);
  });

  afterEach(() => {
    // then / expect these logs for all scenarios within handler
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      2,
      "Request received [requestId: requestId][correlationId: e7fffacb-4575-47ba-b998-e0169945cdd5][userId: ABCD][channel: HRT_ONLINE]",
    );
  });

  it.each([JSON.stringify({}), null])(
    "should respond with a bad request error if req body is %s",
    async (eventBody) => {
      // given
      event.body = eventBody;

      // when
      const result = await handler(event);

      // then
      const expectedResponseBody = {
        ...badRequestBaseResponseBody,
        fieldErrors: [
          {
            field: CITIZEN_FIELD_KEY,
            message: MUST_NOT_BE_NULL_OR_EMPTY_VALIDATION_MESSAGE,
          },
          {
            field: "certificate",
            message: MUST_NOT_BE_NULL_OR_EMPTY_VALIDATION_MESSAGE,
          },
        ],
      };
      const expectedResponseBodyString = JSON.stringify(expectedResponseBody);
      expect(result).toEqual({
        body: expectedResponseBodyString,
        statusCode: 400,
      });
      expect(mockIssueCertificate).not.toHaveBeenCalled();
      expect(winstonInfoLoggerSpy).toHaveBeenCalledTimes(3);
      expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
        3,
        `Response: 400 ${expectedResponseBodyString}`,
      );
    },
  );

  it("should respond with a bad request error if request citizen field is empty", async () => {
    // given
    event.body = JSON.stringify({
      citizen: {},
    });

    // when
    const result = await handler(event);

    // then
    const expectedResponseBody = {
      ...badRequestBaseResponseBody,
      fieldErrors: [
        {
          field: CITIZEN_FIELD_KEY + ".userPreference",
          message: MUST_NOT_BE_NULL_OR_EMPTY_VALIDATION_MESSAGE,
        },
        {
          field: "certificate",
          message: MUST_NOT_BE_NULL_OR_EMPTY_VALIDATION_MESSAGE,
        },
      ],
    };
    const expectedResponseBodyString = JSON.stringify(expectedResponseBody);
    expect(result).toEqual({
      body: expectedResponseBodyString,
      statusCode: 400,
    });
    expect(mockIssueCertificate).not.toHaveBeenCalled();
    expect(winstonInfoLoggerSpy).toHaveBeenCalledTimes(3);
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      3,
      `Response: 400 ${expectedResponseBodyString}`,
    );
  });

  it("should respond with a bad request error if request certificate field is empty", async () => {
    // given
    event.body = JSON.stringify({
      certificate: {},
    });

    // when
    const result = await handler(event);

    // then
    const expectedResponseBody = {
      ...badRequestBaseResponseBody,
      fieldErrors: [
        {
          field: CITIZEN_FIELD_KEY,
          message: MUST_NOT_BE_NULL_OR_EMPTY_VALIDATION_MESSAGE,
        },
        {
          field: "certificate.productId",
          message: MUST_NOT_BE_NULL_OR_EMPTY_VALIDATION_MESSAGE,
        },
      ],
    };
    const expectedResponseBodyString = JSON.stringify(expectedResponseBody);
    expect(result).toEqual({
      body: expectedResponseBodyString,
      statusCode: 400,
    });
    expect(mockIssueCertificate).not.toHaveBeenCalled();
    expect(winstonInfoLoggerSpy).toHaveBeenCalledTimes(3);
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      3,
      `Response: 400 ${expectedResponseBodyString}`,
    );
  });

  it("should respond with an internal server error", async () => {
    // given
    mockIssueCertificate.mockRejectedValueOnce(Error("Internal Server Error"));

    // when
    const result = await handler(event);

    // then
    expect(mockIssueCertificate).toHaveBeenCalledTimes(1);
    expect(mockIssueCertificate).toHaveBeenCalledWith(
      mockHeaders,
      mockCitizen,
      null,
      mockCertificate,
    );
    expect(result).toMatchInlineSnapshot(`
      {
        "body": "{"status":500,"message":"Internal Server Error","timestamp":"2020-01-01T00:00:00.000Z"}",
        "statusCode": 500,
      }
    `);
  });

  it("should respond with the 200 success response when certificate issued successfully", async () => {
    // given / when
    const result = await handler(event);

    // then
    expect(result).toEqual({
      statusCode: 200,
      body: JSON.stringify(mockSuccessResponse),
    });
    expect(mockIssueCertificate).toHaveBeenCalledTimes(1);
    expect(mockIssueCertificate).toHaveBeenCalledWith(
      mockHeaders,
      mockCitizen,
      null,
      mockCertificate,
    );
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      3,
      "Request body validated",
    );
  });

  it("should ignore the additional headers and pass mandatory headers to handler", async () => {
    // given
    event.headers = {
      ...mockHeaders,
      additionalHeader: "ignored",
    };

    // when
    await handler(event);

    // then
    expect(mockIssueCertificate).toHaveBeenCalledTimes(1);
    expect(mockIssueCertificate).toHaveBeenCalledWith(
      mockHeaders,
      mockCitizen,
      null,
      mockCertificate,
    );
    expect(mockIssueCertificate.mock.calls[0][0]).toMatchObject(mockHeaders);
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      3,
      "Request body validated",
    );
  });
});

describe("handler with partner", () => {
  beforeEach(() => {
    event = JSON.parse(JSON.stringify(mockEventWithPartner));
    mockIssueCertificate.mockResolvedValue(mockSuccessResponseWithPartner);
  });

  afterEach(() => {
    // then / expect these logs for all scenarios for handler with partner test cases
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      2,
      "Request received [requestId: requestId][correlationId: e7fffacb-4575-47ba-b998-e0169945cdd5][userId: ABCD][channel: LIS_STAFF]",
    );
  });

  it("should respond with a bad request error if certificate type is not LIS_HC2 or LIS_HC3", async () => {
    // given
    event.body = JSON.stringify({
      citizen: generateMock(citizenBaseSchema),
      partner: generateMock(citizenBaseSchema),
      certificate: {
        productId: PRODUCT_ID,
        type: CertificateType.HRT_PPC,
      },
    });

    // when
    const result = await handler(event);

    // then
    const expectedResponseBody = {
      ...badRequestBaseResponseBody,
      fieldErrors: [
        {
          field: PARTNER_FIELD_KEY,
          message:
            "partner only applies to LIS_HC2 or LIS_HC3 certificate types",
        },
      ],
    };
    const expectedResponseBodyString = JSON.stringify(expectedResponseBody);
    expect(result).toEqual({
      body: expectedResponseBodyString,
      statusCode: 400,
    });
    expect(mockIssueCertificate).not.toHaveBeenCalled();
    expect(winstonInfoLoggerSpy).toHaveBeenCalledTimes(3);
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      3,
      `Response: 400 ${expectedResponseBodyString}`,
    );
  });

  it.each([CITIZEN_FIELD_KEY, PARTNER_FIELD_KEY])(
    "should respond with a bad request error if %s.userPreference.preference is EMAIL but no emails",
    async (pathKey: string) => {
      // given
      const alternatePathKey =
        pathKey === CITIZEN_FIELD_KEY ? PARTNER_FIELD_KEY : CITIZEN_FIELD_KEY;
      const requestBody = {
        [pathKey]: {
          userPreference: {
            preference: Preference.EMAIL,
          },
        },
        [alternatePathKey]: generateMock(citizenBaseSchema),
        certificate: {
          productId: PRODUCT_ID,
          type: CertificateType.LIS_HC2,
        },
      };
      event.body = JSON.stringify(requestBody);

      // when
      const result = await handler(event);

      // then
      const expectedResponseBody = {
        ...badRequestBaseResponseBody,
        fieldErrors: [
          {
            field: pathKey,
            message: USER_PREFERENCE_NO_EMAIL_VALIDATION_MESSAGE,
          },
        ],
      };
      const expectedResponseBodyString = JSON.stringify(expectedResponseBody);
      expect(result).toEqual({
        body: expectedResponseBodyString,
        statusCode: 400,
      });
      expect(mockIssueCertificate).not.toHaveBeenCalled();
      expect(winstonInfoLoggerSpy).toHaveBeenCalledTimes(3);
      expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
        3,
        `Response: 400 ${expectedResponseBodyString}`,
      );
    },
  );

  it.each([CITIZEN_FIELD_KEY, PARTNER_FIELD_KEY])(
    "should respond with a bad request error if %s.userPreference.preference is EMAIL but emails an empty array",
    async (pathKey: string) => {
      // given
      const alternatePathKey =
        pathKey === CITIZEN_FIELD_KEY ? PARTNER_FIELD_KEY : CITIZEN_FIELD_KEY;
      const requestBody = {
        [pathKey]: {
          emails: [],
          userPreference: {
            preference: Preference.EMAIL,
          },
        },
        [alternatePathKey]: generateMock(citizenBaseSchema),
        certificate: {
          productId: PRODUCT_ID,
          type: CertificateType.LIS_HC2,
        },
      };
      event.body = JSON.stringify(requestBody);

      // when
      const result = await handler(event);

      // then
      const expectedResponseBody = {
        ...badRequestBaseResponseBody,
        fieldErrors: [
          {
            field: pathKey,
            message: USER_PREFERENCE_NO_EMAIL_VALIDATION_MESSAGE,
          },
        ],
      };
      const expectedResponseBodyString = JSON.stringify(expectedResponseBody);
      expect(result).toEqual({
        body: expectedResponseBodyString,
        statusCode: 400,
      });
      expect(mockIssueCertificate).not.toHaveBeenCalled();
      expect(winstonInfoLoggerSpy).toHaveBeenCalledTimes(3);
      expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
        3,
        `Response: 400 ${expectedResponseBodyString}`,
      );
    },
  );

  it.each([CITIZEN_FIELD_KEY, PARTNER_FIELD_KEY])(
    "should respond with a bad request error if %s userPreference is EMAIL but emailAddress is empty",
    async (pathKey: string) => {
      // given
      const alternatePathKey =
        pathKey === CITIZEN_FIELD_KEY ? PARTNER_FIELD_KEY : CITIZEN_FIELD_KEY;
      const requestBody = {
        [pathKey]: {
          emails: [{ emailAddress: undefined }],
          userPreference: {
            preference: Preference.EMAIL,
          },
        },
        [alternatePathKey]: generateMock(citizenBaseSchema),
        certificate: {
          productId: PRODUCT_ID,
          type: CertificateType.LIS_HC2,
        },
      };
      event.body = JSON.stringify(requestBody);

      // when
      const result = await handler(event);

      // then
      const expectedResponseBody = {
        ...badRequestBaseResponseBody,
        fieldErrors: [
          {
            field: pathKey,
            message: USER_PREFERENCE_NO_EMAIL_VALIDATION_MESSAGE,
          },
        ],
      };
      const expectedResponseBodyString = JSON.stringify(expectedResponseBody);
      expect(result).toEqual({
        body: expectedResponseBodyString,
        statusCode: 400,
      });
      expect(mockIssueCertificate).not.toHaveBeenCalled();
      expect(winstonInfoLoggerSpy).toHaveBeenCalledTimes(3);
      expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
        3,
        `Response: 400 ${expectedResponseBodyString}`,
      );
    },
  );

  it.each([CITIZEN_FIELD_KEY, PARTNER_FIELD_KEY])(
    "should respond with a 200 if %s userPreference is POSTAL and emails an empty array",
    async (pathKey: string) => {
      // given
      const alternatePathKey =
        pathKey === CITIZEN_FIELD_KEY ? PARTNER_FIELD_KEY : CITIZEN_FIELD_KEY;
      const mockRequestBodyData = {
        [pathKey]: {
          emails: [],
          userPreference: {
            preference: Preference.POSTAL,
          },
        },
        [alternatePathKey]: generateMock(citizenBaseSchema),
        certificate: {
          productId: PRODUCT_ID,
          type: CertificateType.LIS_HC2,
        },
      };
      event.body = JSON.stringify(mockRequestBodyData);

      // when
      const result = await handler(event);

      // then
      expect(result).toEqual({
        body: JSON.stringify(mockSuccessResponseWithPartner),
        statusCode: 200,
      });
      expect(mockIssueCertificate).toHaveBeenCalledTimes(1);
      expect(mockIssueCertificate).toHaveBeenCalledWith(
        mockMandatoryLisHeaders,
        mockRequestBodyData[CITIZEN_FIELD_KEY],
        mockRequestBodyData[PARTNER_FIELD_KEY],
        mockRequestBodyData.certificate,
      );
      expect(winstonInfoLoggerSpy).toHaveBeenCalledTimes(3);
      expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
        3,
        "Request body validated",
      );
    },
  );

  it("should respond with an internal server error", async () => {
    // given
    mockIssueCertificate.mockRejectedValueOnce(Error("Internal Server Error"));

    // when
    const result = await handler(event);

    // then
    expect(result).toMatchInlineSnapshot(`
      {
        "body": "{"status":500,"message":"Internal Server Error","timestamp":"2020-01-01T00:00:00.000Z"}",
        "statusCode": 500,
      }
    `);
    expect(mockIssueCertificate).toHaveBeenCalledTimes(1);
    expect(mockIssueCertificate).toHaveBeenCalledWith(
      mockMandatoryLisHeaders,
      mockCitizen,
      mockPartnerCitizen,
      mockLisCertificate,
    );
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      3,
      "Request body validated",
    );
  });

  it("should respond with the 200 success response when certificate issued successfully with partner", async () => {
    // given / when
    const result = await handler(event);

    // then
    expect(result).toEqual({
      statusCode: 200,
      body: JSON.stringify(mockSuccessResponseWithPartner),
    });
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      3,
      "Request body validated",
    );
  });
});
