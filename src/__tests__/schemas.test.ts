import { generateMock } from "@anatine/zod-mock";
import {
  certificateSchema,
  citizenBaseSchema,
  comparisonSchema,
  payloadSchema,
  userPreferenceSchema,
} from "../schemas";
import { dissocPath } from "ramda";
import { mockCitizen } from "../__mocks__/citizen/citizen-mocks";
import { customErrorMap } from "../index";
import {
  CITIZEN_FIELD_KEY,
  MUST_NOT_BE_NULL_OR_EMPTY_VALIDATION_MESSAGE,
  PARTNER_FIELD_KEY,
  PRODUCT_ID,
  USER_PREFERENCE_NO_EMAIL_VALIDATION_MESSAGE,
} from "./utils/test-constants";
import {
  CertificateType,
  Preference,
} from "@nhsbsa/health-charge-exemption-npm-common-models";

describe("payloadSchema", () => {
  const validMockData = generateMock(payloadSchema);

  it("should parse valid json when all fields valid per request", async () => {
    // given
    validMockData.certificate.type = CertificateType.HRT_PPC;
    validMockData.partner = undefined;

    // when
    const validationResult = payloadSchema.safeParse(validMockData, {
      errorMap: customErrorMap,
    });

    // then
    expect(validationResult.success).toBeTruthy();
    expect(validationResult.data).toStrictEqual(validMockData);
    expect(validationResult).toStrictEqual({
      data: validMockData,
      success: true,
    });
  });

  it("should parse valid json when all required fields present", async () => {
    // given
    const mockDataWithOnlyRequiredFields = {
      citizen: {
        userPreference: {
          preference: Preference.POSTAL,
        },
      },
      certificate: {
        productId: "123",
      },
    };

    // when
    const validationResult = payloadSchema.safeParse(
      mockDataWithOnlyRequiredFields,
      {
        errorMap: customErrorMap,
      },
    );

    // then
    expect(validationResult.success).toBeTruthy();
    expect(validationResult.data).toStrictEqual(mockDataWithOnlyRequiredFields);
    expect(validationResult).toStrictEqual({
      data: mockDataWithOnlyRequiredFields,
      success: true,
    });
  });

  it.each([CertificateType.LIS_HC2, CertificateType.LIS_HC3])(
    "should parse valid json when all required fields and partner with type %s present",
    async (certificateType: CertificateType) => {
      // given
      const mockDataWithPartnerOnlyRequiredFields = {
        citizen: {
          userPreference: {
            preference: Preference.POSTAL,
          },
        },
        partner: {
          userPreference: {
            preference: Preference.POSTAL,
          },
        },
        certificate: {
          type: certificateType,
          productId: "123",
        },
      };

      // when
      const validationResult = payloadSchema.safeParse(
        mockDataWithPartnerOnlyRequiredFields,
        {
          errorMap: customErrorMap,
        },
      );

      // then
      expect(validationResult.success).toBeTruthy();
      expect(validationResult.data).toStrictEqual(
        mockDataWithPartnerOnlyRequiredFields,
      );
      expect(validationResult).toStrictEqual({
        data: mockDataWithPartnerOnlyRequiredFields,
        success: true,
      });
    },
  );

  it.each([CertificateType.LIS_HC2, CertificateType.LIS_HC3])(
    "should parse valid json when all fields valid per request with partner and lis type",
    async (certificateType: CertificateType) => {
      // given
      validMockData.certificate.type = certificateType;

      // when
      const validationResult = payloadSchema.safeParse(validMockData, {
        errorMap: customErrorMap,
      });

      // then
      expect(validationResult.success).toBeTruthy();
      expect(validationResult.data).toStrictEqual(validMockData);
      expect(validationResult).toStrictEqual({
        data: validMockData,
        success: true,
      });
    },
  );

  it.each([CITIZEN_FIELD_KEY, "certificate"])(
    "should return error when missing %s",
    async (pathKey: string) => {
      // given
      // using dissocPath instead of omit to negate ts error
      const mockData = dissocPath([pathKey], validMockData);

      // when
      const validationResult = payloadSchema.safeParse(mockData, {
        errorMap: customErrorMap,
      });

      // then
      expect(validationResult.success).toBeFalsy();
      expect(validationResult.data).toBeUndefined();
      expect(validationResult.error).toBeDefined();
      expect(validationResult.error?.issues.length).toEqual(1);
      expect(validationResult.error?.issues[0]).toStrictEqual({
        code: "invalid_type",
        expected: "object",
        message: MUST_NOT_BE_NULL_OR_EMPTY_VALIDATION_MESSAGE,
        path: [pathKey],
        received: "undefined",
      });
    },
  );

  it("should validate to true when valid citizen and certificate but no partner", () => {
    // given
    // using dissocPath instead of omit to negate ts error
    const mockData = dissocPath([PARTNER_FIELD_KEY], validMockData);

    // when
    const validationResult = payloadSchema.safeParse(mockData, {
      errorMap: customErrorMap,
    });

    // then
    expect(validationResult.success).toBeTruthy();
    expect(validationResult.data).toBeDefined();
    expect(validationResult.data).toStrictEqual(mockData);
    expect(validationResult).toStrictEqual({
      data: mockData,
      success: true,
    });
  });

  it("should return error for citizen and certificate when empty payload data", () => {
    // given / when
    const validationResult = payloadSchema.safeParse(
      {},
      {
        errorMap: customErrorMap,
      },
    );

    // then
    expect(validationResult.success).toBeFalsy();
    expect(validationResult.data).toBeUndefined();
    expect(validationResult.error).toBeDefined();
    expect(validationResult.error?.issues.length).toEqual(2);
    expect(validationResult.error?.issues).toStrictEqual([
      {
        code: "invalid_type",
        expected: "object",
        message: MUST_NOT_BE_NULL_OR_EMPTY_VALIDATION_MESSAGE,
        path: [CITIZEN_FIELD_KEY],
        received: "undefined",
      },
      {
        code: "invalid_type",
        expected: "object",
        message: MUST_NOT_BE_NULL_OR_EMPTY_VALIDATION_MESSAGE,
        path: ["certificate"],
        received: "undefined",
      },
    ]);
  });

  it.each([CITIZEN_FIELD_KEY, PARTNER_FIELD_KEY])(
    "should return error when missing %s.userPreference",
    async (pathKey: string) => {
      // given
      const validMockDataWithPartner = {
        ...validMockData,
        partner: generateMock(citizenBaseSchema),
      };
      const mockData = dissocPath(
        [pathKey, "userPreference"],
        validMockDataWithPartner,
      );

      // when
      const validationResult = payloadSchema.safeParse(mockData, {
        errorMap: customErrorMap,
      });

      // then
      expect(validationResult.success).toBeFalsy();
      expect(validationResult.data).toBeUndefined();
      expect(validationResult.error).toBeDefined();
      expect(validationResult.error?.issues.length).toEqual(1);
      expect(validationResult.error?.issues[0]).toStrictEqual({
        code: "invalid_type",
        expected: "object",
        message: MUST_NOT_BE_NULL_OR_EMPTY_VALIDATION_MESSAGE,
        path: [pathKey, "userPreference"],
        received: "undefined",
      });
    },
  );

  it.each([CITIZEN_FIELD_KEY, PARTNER_FIELD_KEY])(
    "should return error when missing %s.userPreference.preference",
    async (pathKey: string) => {
      // given
      const validMockDataWithPartner = {
        ...validMockData,
        partner: generateMock(citizenBaseSchema),
      };
      const mockData = dissocPath(
        [pathKey, "userPreference", "preference"],
        validMockDataWithPartner,
      );

      // when
      const validationResult = payloadSchema.safeParse(mockData, {
        errorMap: customErrorMap,
      });

      // then
      expect(validationResult.success).toBeFalsy();
      expect(validationResult.data).toBeUndefined();
      expect(validationResult.error).toBeDefined();
      expect(validationResult.error?.issues.length).toEqual(1);
      expect(validationResult.error?.issues[0]).toStrictEqual({
        code: "invalid_type",
        expected: "'EMAIL' | 'POSTAL'",
        message: MUST_NOT_BE_NULL_OR_EMPTY_VALIDATION_MESSAGE,
        path: [pathKey, "userPreference", "preference"],
        received: "undefined",
      });
    },
  );

  it.each([CITIZEN_FIELD_KEY, PARTNER_FIELD_KEY])(
    "should return error when %s.userPreference.preference is EMAIL and no email address",
    async (pathKey: string) => {
      // given
      const alternatePathKey =
        pathKey === CITIZEN_FIELD_KEY ? PARTNER_FIELD_KEY : CITIZEN_FIELD_KEY;
      const mockPayloadSchemaData = {
        [pathKey]: {
          emails: [],
          userPreference: {
            preference: Preference.EMAIL,
          },
        },
        [alternatePathKey]: generateMock(citizenBaseSchema),
        certificate: {
          productId: PRODUCT_ID,
          type: CertificateType.LIS_HC2,
        },
      };

      // when
      const validationResult = payloadSchema.safeParse(mockPayloadSchemaData, {
        errorMap: customErrorMap,
      });

      // then
      expect(validationResult.success).toBeFalsy();
      expect(validationResult.data).toBeUndefined();
      expect(validationResult.error).toBeDefined();
      expect(validationResult.error?.issues.length).toEqual(1);
      expect(validationResult.error?.issues).toStrictEqual([
        {
          code: "custom",
          message: USER_PREFERENCE_NO_EMAIL_VALIDATION_MESSAGE,
          path: [pathKey],
        },
      ]);
    },
  );

  it("should return error when valid partner but certificate is not LIS_HC2 or LIS_HC3", () => {
    // given
    const mockPayloadSchemaData = {
      citizen: generateMock(citizenBaseSchema),
      partner: generateMock(citizenBaseSchema),
      certificate: {
        productId: PRODUCT_ID,
        type: CertificateType.HRT_PPC,
      },
    };

    // when
    const validationResult = payloadSchema.safeParse(mockPayloadSchemaData, {
      errorMap: customErrorMap,
    });

    // then
    expect(validationResult.success).toBeFalsy();
    expect(validationResult.data).toBeUndefined();
    expect(validationResult.error).toBeDefined();
    expect(validationResult.error?.issues.length).toEqual(1);
    expect(validationResult.error?.issues).toStrictEqual([
      {
        code: "custom",
        message: "partner only applies to LIS_HC2 or LIS_HC3 certificate types",
        path: [PARTNER_FIELD_KEY],
      },
    ]);
  });

  it("should return multiple preference error when partner and citizen preference is EMAIL and no email address", () => {
    // given
    const mockPayloadSchemaData = {
      citizen: {
        emails: [],
        userPreference: {
          preference: Preference.EMAIL,
        },
      },
      partner: {
        emails: [],
        userPreference: {
          preference: Preference.EMAIL,
        },
      },
      certificate: {
        productId: PRODUCT_ID,
        type: CertificateType.LIS_HC2,
      },
    };

    // when
    const validationResult = payloadSchema.safeParse(mockPayloadSchemaData, {
      errorMap: customErrorMap,
    });

    // then
    expect(validationResult.success).toBeFalsy();
    expect(validationResult.data).toBeUndefined();
    expect(validationResult.error).toBeDefined();
    expect(validationResult.error?.issues.length).toEqual(2);
    expect(validationResult.error?.issues).toStrictEqual([
      {
        code: "custom",
        message: USER_PREFERENCE_NO_EMAIL_VALIDATION_MESSAGE,
        path: [CITIZEN_FIELD_KEY],
      },
      {
        code: "custom",
        message: USER_PREFERENCE_NO_EMAIL_VALIDATION_MESSAGE,
        path: [PARTNER_FIELD_KEY],
      },
    ]);
  });
});

describe("userPreferenceSchema", () => {
  it("should return error when invalid preference", () => {
    // given
    const userPreference = {
      preference: "invalid",
    };

    // when
    const validationResult = userPreferenceSchema.safeParse(userPreference, {
      errorMap: customErrorMap,
    });

    // then
    expect(validationResult.success).toBeFalsy();
    expect(validationResult.data).toBeUndefined();
    expect(validationResult.error).toBeDefined();
    expect(validationResult.error?.issues.length).toEqual(1);
    expect(validationResult.error?.issues[0]).toStrictEqual({
      code: "invalid_enum_value",
      options: [Preference.EMAIL, Preference.POSTAL],
      message:
        "Invalid enum value. Expected 'EMAIL' | 'POSTAL', received 'invalid'",
      path: ["preference"],
      received: "invalid",
    });
  });

  it.each([Preference.EMAIL, Preference.POSTAL])(
    "should successfully validate when valid preference data when preference is %s",
    (preference: Preference) => {
      // given
      const userPreferenceMockData = {
        preference: preference,
      };

      // when
      const validationResult = userPreferenceSchema.safeParse(
        userPreferenceMockData,
        {
          errorMap: customErrorMap,
        },
      );

      // then
      expect(validationResult.success).toBeTruthy();
      expect(validationResult.data).toEqual(userPreferenceMockData);
      expect(validationResult.error).toBeUndefined();
      expect(validationResult).toStrictEqual({
        data: userPreferenceMockData,
        success: true,
      });
    },
  );
});

describe("certificateSchema", () => {
  it("should return error when no productId in certificate", () => {
    // given / when
    const validationResult = certificateSchema.safeParse(
      {},
      {
        errorMap: customErrorMap,
      },
    );

    // then
    expect(validationResult.success).toBeFalsy();
    expect(validationResult.data).toBeUndefined();
    expect(validationResult.error).toBeDefined();
    expect(validationResult.error?.issues.length).toEqual(1);
    expect(validationResult.error?.issues[0]).toStrictEqual({
      code: "invalid_type",
      expected: "string",
      message: MUST_NOT_BE_NULL_OR_EMPTY_VALIDATION_MESSAGE,
      path: ["productId"],
      received: "undefined",
    });
  });

  it("should successfully validate when valid certificate data", () => {
    // given
    const certificateMockData = generateMock(certificateSchema);

    // when
    const validationResult = certificateSchema.safeParse(certificateMockData, {
      errorMap: customErrorMap,
    });

    // then
    expect(validationResult.success).toBeTruthy();
    expect(validationResult.data).toEqual(certificateMockData);
    expect(validationResult.error).toBeUndefined();
    expect(validationResult).toStrictEqual({
      data: certificateMockData,
      success: true,
    });
  });
});

describe("comparisonSchema", () => {
  it("should successfully parse necessary fields when valid citizen data", () => {
    // given / when
    const parsedResult = comparisonSchema.parse(mockCitizen);

    // then
    expect(parsedResult).toMatchInlineSnapshot(`
    {
      "addresses": [
        {
          "addressLine1": "Stella House",
          "addressLine2": "Goldcrest Way",
          "country": "England",
          "postcode": "NE15 8NY",
          "townOrCity": "Newcastle upon Tyne",
        },
      ],
      "dateOfBirth": "2022-11-18",
      "emails": [
        {
          "emailAddress": "test@example.com",
        },
      ],
      "firstName": "John",
      "lastName": "Smith",
      "nhsNumber": "5165713040",
    }
    `);
  });

  it("should transform undefined emails to empty array when parsed", () => {
    // given
    const mockCitizenUndefinedEmail = {
      ...mockCitizen,
      emails: undefined,
    };

    // when
    const parsedResult = comparisonSchema.parse(mockCitizenUndefinedEmail);

    // then
    expect(parsedResult).toMatchInlineSnapshot(`
    {
      "addresses": [
        {
          "addressLine1": "Stella House",
          "addressLine2": "Goldcrest Way",
          "country": "England",
          "postcode": "NE15 8NY",
          "townOrCity": "Newcastle upon Tyne",
        },
      ],
      "dateOfBirth": "2022-11-18",
      "emails": [],
      "firstName": "John",
      "lastName": "Smith",
      "nhsNumber": "5165713040",
    }
    `);
  });
});
