import { APIGatewayEvent } from "aws-lambda";
import {
  loggerWithContext,
  getHeaderValue,
  setCorrelationId,
} from "@nhsbsa/health-charge-exemption-npm-logging";
import {
  successResponse,
  errorResponse,
  BadRequest,
  transformHeadersAndReturnMandatory,
  FieldError,
  MandatoryHeaders,
} from "@nhsbsa/health-charge-exemption-npm-utils-rest";
import { issueCertificate } from "./hce-issue-certificate";
import { PayloadBody, payloadSchema } from "./schemas";
import { z } from "zod";

const logRequest = (headers: MandatoryHeaders, requestId) => {
  const correlationId = getHeaderValue(headers, "correlation-id");
  const channel = getHeaderValue(headers, "channel");
  const userId = getHeaderValue(headers, "user-id");

  loggerWithContext().info(
    `Request received [requestId: ${requestId}][correlationId: ${correlationId}][userId: ${userId}][channel: ${channel}]`,
  );
};

/**
 * A customised error map to allow for the correct default error message when invalid type.
 * Used when an object in the request is missing, null or invalid for all types.
 */
export const customErrorMap: z.ZodErrorMap = (error, context) => {
  if (error.code === z.ZodIssueCode.invalid_type) {
    return { message: "must not be null or empty" };
  }
  return { message: context.defaultError };
};

export async function handler(event: APIGatewayEvent) {
  try {
    loggerWithContext().info("hce-issue-certificate-lambda-v2");
    const {
      headers,
      body,
      requestContext: { requestId },
    } = event;

    setCorrelationId(getHeaderValue(headers, "correlation-id") as string);

    const mandatoryHeaders: MandatoryHeaders =
      transformHeadersAndReturnMandatory(headers);

    logRequest(mandatoryHeaders, requestId);

    const payloadBody: PayloadBody = JSON.parse(body || "{}");

    const validationResult = payloadSchema.safeParse(payloadBody, {
      errorMap: customErrorMap,
    });

    // if the schema validation has failed throw a BadRequest
    if (!validationResult.success) {
      /*
       * Processes the errors from the validation result and maps them to a more readable format.
       * Example: { path: ['user', 'name'], message: 'Name is required' }, becomes: { field: 'user.name', message: 'Name is required' }.
       */
      const fieldErrors: FieldError[] = validationResult.error.errors.map(
        (error) => ({
          field: error.path.join("."),
          message: error.message,
        }),
      );
      throw new BadRequest(
        "There were validation issues with the request",
        new Date(),
        fieldErrors,
      );
    }

    const { citizen, partner = null, certificate } = payloadBody;

    loggerWithContext().info("Request body validated");

    const result = await issueCertificate(
      mandatoryHeaders,
      citizen,
      partner,
      certificate,
    );
    return successResponse(200, result);
  } catch (error) {
    loggerWithContext().error({ message: error });
    return errorResponse(error);
  }
}
